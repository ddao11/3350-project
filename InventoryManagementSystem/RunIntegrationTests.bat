REM @echo off

call setClasspath

REM @echo on

REM java junit.textui.TestRunner tests.AllTests

java org.junit.runner.JUnitCore Tests.AllIntegrationTests

call RestoreDB

pause