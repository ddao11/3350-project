package InventoryListPage;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

import BusinessLogic.AccessDB;
import Database.Search;
import DomainObj.Inventory;
import acceptanceTests.Register;

public class InventoryListPage 
{
	private Shell shell;
	private Table table;
	
	protected Text getInventoryText;
	protected Text getInventoryByItem;
	protected Combo getSearchInventoryCombo;
	protected Combo getInventoryCombo;
	
	private Button changeStatus;
	private Button deleteItem;
	private Button exit;
	private Button backToMain;
	private Button search;
	private static boolean isOpen;
	
	public InventoryListPage()
	{
		Register.newWindow(this);
        Display.getDefault();
        if (!isOpen) {
        	isOpen = true;
        	build();
        }
	}
	
	public void build()
	{
		shell = new Shell();
		shell.setSize(600, 650);
		shell.setLocation(150,100);
		shell.setText("Inventory List Page");
		shell.addListener(SWT.Close,  new Listener() {
			public void handleEvent(Event event) {
				isOpen = false;
				shell.setVisible(false);
				shell.dispose();
			}
		});
		setUpTable();
		setUpBotton();
			
		loadList();
		
		shell.open();
	}
	
	private void setUpTable()
	{
		table = new Table( shell,SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		table.setBounds(40, 40, 400, 330);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		
		
		final TableColumn id = new TableColumn(table, SWT.NONE);
		id.setWidth(110);
		id.setText("Inventory ID");
		
		final TableColumn itemName = new TableColumn(table, SWT.NONE);
		itemName.setWidth(140);
		itemName.setText("Item Name");
		
		final TableColumn type = new TableColumn(table, SWT.NONE);
		type.setWidth(140);
		type.setText("status");
		
	}
	public Combo setDropDown(int x,int y,int w,int h,String[] items)
	{
	    Combo box = new Combo(shell, SWT.READ_ONLY);
	    box.setBounds(x, y, w, h);    
	    box.setItems(items);
	    
	    return box;
	}
	private void setUpBotton()
	{
		deleteItem = new Button(shell, SWT.NONE);
		deleteItem.setBounds(460, 180, 120, 40);
		deleteItem.setText("Delete");
		
		deleteItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				deleteItems();
			}
		});
		
		exit = new Button(shell, SWT.NONE);
		exit.setBounds(460, 300, 120, 40);
		exit.setText("Exit");
		
		exit.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				//shell.dispose();
				confirm();
			}
		});
		
		String[] types_for_change = { "Available","SoldOnEtsy","SoldOther", "Lost","Damaged","Donated" };
		getInventoryCombo = setDropDown(220, 400, 220, 40,types_for_change);
		
		final Label changeStatusLabel= new Label(shell, SWT.BORDER);
		changeStatusLabel.setBounds(40, 400, 150, 30);
		changeStatusLabel.setText("Change Status:");
		
		changeStatus = new Button(shell, SWT.NONE);
		changeStatus.setBounds(460, 395, 120, 30);
		changeStatus.setText("Change");
		
		changeStatus.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				String inventoryStatus=getInventoryCombo.getText();
				if (inventoryStatus != null && inventoryStatus.length() > 0)
					changeInventoryStatus(inventoryStatus);
			}
		});
		
		
		Label line = new Label(shell, SWT.SEPARATOR | SWT.HORIZONTAL);
		line.setBounds(40, 430, 535, 30);
		
		final Label getInventoryByIdLabel= new Label(shell, SWT.BORDER);
		getInventoryByIdLabel.setBounds(40, 460, 150, 30);
		getInventoryByIdLabel.setText("Search Inventory By Id:");
		
		getInventoryText = new Text(shell, SWT.BORDER);
		getInventoryText.setTextLimit(30);
		getInventoryText.setBounds(220, 460, 220, 30);
		
		
		backToMain = new Button(shell, SWT.NONE);
		backToMain.setBounds(460, 460, 120, 30);
		backToMain.setText("All Inventory");
		backToMain.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				loadList();
			}
		});
		
		
		final Label getInventoryByStatusLabel= new Label(shell, SWT.BORDER);
		getInventoryByStatusLabel.setBounds(40, 510, 150, 30);
		getInventoryByStatusLabel.setText("Search Inventory By Status:");
		
		String[] types = { "All","Available","SoldOnEtsy","SoldOther", "Lost","Damaged","Donated" };
		getSearchInventoryCombo = setDropDown(220, 510, 220, 30,types);
		
		
		
		final Label getInventoryByItemLabel= new Label(shell, SWT.BORDER);
		getInventoryByItemLabel.setBounds(40, 555, 150, 30);
		getInventoryByItemLabel.setText("Search Inventory By Item:");
		
		getInventoryByItem = new Text(shell, SWT.BORDER);
		getInventoryByItem.setTextLimit(30);
		getInventoryByItem.setBounds(220, 555, 220, 30);
		
		search = new Button(shell, SWT.NONE);
		search.setBounds(460, 555, 120, 30);
		search.setText("Search");
		
		search.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				ArrayList<Inventory> list=null;
				Search s=new Search();
				
				String inventoryID = getInventoryText.getText();
				String inventoryStatus = getSearchInventoryCombo.getText();
				String inventoryName = getInventoryByItem.getText();
				if(!inventoryID.equals("")){
					try{
						Integer.parseInt(inventoryID);
					}catch(Exception e1){
						displayInfoDialog("Please input vaild number");
						return;
					}
				}
						
				if(inventoryID.equals("")&&inventoryStatus.equals("")&&inventoryName.equals("")){
					displayInfoDialog("Please input something");
				}
				else
				{
					if(inventoryStatus.equals("All")){
						inventoryStatus="";
					}
					list=s.getItemByInput(inventoryID, inventoryName, inventoryStatus);
					loadByList(list);
				}
			}
		});

		
		
	}
	
	private void displayInfoDialog(String msg){
		MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK );
		messageBox.setText("Information"); 
		messageBox.setMessage(msg);
		messageBox.open();
	}
	
	private boolean confirmDeleteItems(){
		MessageBox messageBox = new MessageBox(shell,SWT.ICON_QUESTION | SWT.APPLICATION_MODAL | SWT.YES | SWT.NO);
		messageBox.setText("Information"); 
		messageBox.setMessage("Are you sure you want to delete \n");
		boolean result=false;
		if(messageBox.open() == SWT.YES){
			result=true;
		}else if(messageBox.open() == SWT.NO){
			result=false;
		}
		return result;
	}
	

	
	private void confirm()
	{
		MessageBox messageBox = new MessageBox(shell,SWT.ICON_QUESTION | SWT.APPLICATION_MODAL | SWT.YES | SWT.NO);
		messageBox.setText("Information"); 
		messageBox.setMessage("Are you sure you want to exit?");
		if(messageBox.open() == SWT.YES)
		{
			isOpen = false;
			shell.setVisible(false);
			shell.dispose();
		}
	}
	
	private void changeInventoryStatus(String status){

		if(noItemsAreChecked()){
			displayInfoDialog("No items are checked to change status");
		}
		else {
			AccessDB.changeStatus(table, status);
		}
	}
	
	public void loadList()
	{
		AccessDB.loadInventoryList(table);
	}
	
	//load list by inventory ArrayList.
	public void loadByList(ArrayList<Inventory> list){
		AccessDB.loadByList(list, table);
	}
	
	private void deleteItems()
	{
		if(confirmDeleteItems()){
			if (noItemsAreChecked()) {
				displayInfoDialog("No items are checked to delete");
			}
			else {
				AccessDB.deleteInventoryItems(table);
			}
		}
	}
	
	private boolean noItemsAreChecked() {
		boolean itemChecked = false;
		for (int i = 0; i < table.getItemCount(); i++) {
			if (table.getItem(i).getChecked())
				itemChecked = true;
		}
		return !itemChecked;
	}

	public Table getTable() {
		return table;
	}
}
