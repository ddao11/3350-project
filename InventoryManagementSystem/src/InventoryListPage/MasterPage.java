package InventoryListPage;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Combo;

import BusinessLogic.AccessDB;
import Database.DBControllerInterface;
import Database.DBHolder;
import DomainObj.Material;
import acceptanceTests.Register;

import java.util.ArrayList;

public class MasterPage {
	protected Display display;
	protected Shell shell;
	
	private Table table;
	
	private Button close;
	
	
	protected Text nameText;
	protected Combo type;
	protected Text price;
	
	protected ArrayList<Material> material;
		
	public void createWindow()
	{		
		setUpLable();
		setUpText();
		setUpButton();
		setUpTable();
	}
	
	private void setUpLable()
	{
		final Label title = new Label(shell, SWT.BORDER);
		title.setBounds(115, 20, 150, 25);
		title.setText("Item infomation:");
		
		final Label nameIDLabel = new Label(shell, SWT.NONE);
		nameIDLabel.setBounds(115, 55, 65, 25);
		nameIDLabel.setText("Name");
		
		final Label typeIDLabel = new Label(shell, SWT.NONE);
		typeIDLabel.setBounds(115, 90, 65, 25);
		typeIDLabel.setText("Type");
		
		final Label pIDLabel = new Label(shell, SWT.NONE);
		pIDLabel.setBounds(115, 125, 65, 25);
		pIDLabel.setText("Price");
		
		final Label mTable = new Label(shell, SWT.BORDER);
		mTable.setBounds(115, 170, 110, 25);
		mTable.setText("Material List:");
	}
	
	private void setUpText()
	{
		nameText = new Text(shell, SWT.BORDER);
		nameText.setTextLimit(30);
		nameText.setBounds(180, 55, 200, 25);
		
		String[] types = { "Journal", "Sketchbook", "Floppy Disk Notebook", "Duct Tape Book","Wood Book","Leather Card Holder","Keychain Book","RFID Card Sleeve" };
		type = setDropDown(180,90,200,25,types);

		price = new Text(shell, SWT.BORDER);
		price.setTextLimit(30);
		price.setBounds(180, 125, 200, 25);
	}
	
	private void setUpButton()
	{
		close = new Button(shell, SWT.NONE);
		close.setBounds(290, 500, 70, 30);
		close.setText("close");
		close.addSelectionListener(new SelectionAdapter()
 		{
			public void widgetSelected(SelectionEvent e)
			{	
				shell.setVisible(false);
				shell.dispose();
			}
		});
	}
	
	private void setUpTable()
	{
		table = new Table(shell,SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		table.setBounds(115, 215, 300, 250);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		
		final TableColumn id = new TableColumn(table, SWT.NONE);
		id.setWidth(25);
		id.setText("*");
		
		final TableColumn mName = new TableColumn(table, SWT.NONE);
		mName.setWidth(145);
		mName.setText("Material");
		
		final TableColumn mCost = new TableColumn(table, SWT.NONE);
		mCost.setWidth(125);
		mCost.setText("Cost");
		
		setTable();
	}
	
	protected void setTable()
	{
		TableItem row;
		
		
		material = AccessDB.getAllMaterials();
		
		for(int i=0;i<material.size();i++)
		{
			row = new TableItem(table, SWT.NONE);
			row.setText(new String[] {"" , material.get(i).getName(),""+material.get(i).getCost()});
		}
		
		
		
	}
	
	protected void getMaterials()
	{
		material = new ArrayList<Material>();
		for(int i=0;i<table.getItemCount();i++)
		{
			if(table.getItem(i).getChecked())
			{
				material.add(new Material(table.getItem(i).getText(1),Double.parseDouble(table.getItem(i).getText(2))));
			}
		}
	}
	
	protected void setMaterials()
	{
		int index=0;
		if(!material.isEmpty())
		{
			for(int curr=0;curr<table.getItemCount();curr++)
			{
				if(index<material.size())
				{
					if(material.get(index).equals(new Material(table.getItem(curr).getText(1),Double.parseDouble(table.getItem(curr).getText(2)))))
					{
						table.getItem(curr).setChecked(true);
						index++;
					}
					else
					{
						table.getItem(curr).setChecked(false);
					}
				}
				
			}
		}
	}
	
	
	
	public void errorMessage(String prompt)
	{
		MessageBox dialog = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK );
		dialog.setText("infomation");
		dialog.setMessage(prompt);
		dialog.open();
	}
	
	public Combo setDropDown(int x,int y,int w,int h,String[] items)
	{
	    Combo box = new Combo(shell, SWT.READ_ONLY);
	    box.setBounds(x, y, w, h);    
	    box.setItems(items);
	    
	    return box;
	}
	
	public void displayInvalid()
	{
		String message = "The infomation is invalid : \n\n";
		int count = 1;
		if(nameText.getText().isEmpty())
		{
			message += count + ". You didn't enter the Name of the item.\n";
			count++;
		}
		
		if(type.getText().isEmpty())
		{
			message += count +". You didn't select the type of the item.\n";
			count++;
		}
		
		if(price.getText().isEmpty())
		{
			message += count +". You didn't enter the price.\n";
			count++;
		}	
		else if(!validDouble())
		{
			message += count +". The input of price should contain a number.\n";
			count++;
		}
		
		if(material.size()==0)
		{
			message += count +". You should select at least one material.\n";
			count++;
		}
		
		errorMessage(message);
	}
	
	public boolean isValid()
	{
		boolean result = true;
		
		if(nameText.getText().isEmpty()||type.getText().isEmpty()||price.getText().isEmpty()||!validDouble()||material.size()==0)
		{
			result = false;
		}
		return result;
	}
	
	private boolean validDouble()
	{
		boolean result;
		try
		{
			Double.parseDouble(price.getText());
			result = true;
		}
		catch(NumberFormatException error)
		{
			result = false;
		}
		return result;
	}
	
}
