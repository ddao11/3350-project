package InventoryListPage;

import BusinessLogic.AccessDB;
import Database.DBControllerInterface;
import Database.DBHolder;
import Database.DBObj;
import DomainObj.Item;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class ViewPage extends MasterPage {

	private Button edit;
	
	public ViewPage(final ListPage parent,DBObj dbObj)
	{
		Item item = (Item)dbObj.getData();
		final int id = dbObj.getId();
		display = Display.getDefault();
        createWindow();
       
        nameText.setText(item.getName());
		type.setText(item.getType());
		price.setText(""+item.getPrice());
		
		material = item.getMaterials();
		
		setMaterials();
		
		edit.addSelectionListener(new SelectionAdapter()
 		{
			public void widgetSelected(SelectionEvent e)
			{	
				getMaterials();
				if(isValid())
				{
					Item item = new Item (nameText.getText(), type.getText(), material, Double.parseDouble(price.getText()));
					AccessDB.updateItem(id, item);
					/*DBControllerInterface db = DBHolder.getDb();
								
					db.connect();
					
					dbObj = db.getDbObj(id);

					dbObj.setData(new Item (nameText.getText(), type.getText(), material, Double.parseDouble(price.getText())));
					db.updateItem(dbObj);
					
					db.close();*/
					
					shell.dispose();
					parent.loadList();
				}
				else
				{
					displayInvalid();
				}
				
			}
		});
		
        shell.open();
	}
	
	public void createWindow()
	{
		shell = new Shell();
		shell.setSize(500, 615);
		shell.setLocation(200,125);
		shell.setText("View Page");
		
		super.createWindow();
		
		edit = new Button(shell, SWT.NONE);
		edit.setBounds(130, 500, 70, 30);
		edit.setText("Edit");
		
	}
	
}