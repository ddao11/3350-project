package InventoryListPage;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import BusinessLogic.AccessDB;
import Database.DBControllerInterface;
import Database.DBHolder;
import Database.DBObj;
import DomainObj.DeleteItem;
import DomainObj.Item;
import acceptanceTests.Register;

public class ListPage 
{
	private Shell shell;
	private Table table;
	
	private Button create;
	private Button addInventory;
	private Button viewItem;
	private Button deleteItem;
	private Button exit;
	private StartScreen parent;
	private static boolean isOpen;
	
	/*public ListPage(final InventoryListPage parent)
	{
        this.parent=parent;
		build();
	}*/
	
	public ListPage(final StartScreen start) {
		this.parent = start;
		
		if (!isOpen) {
			build();
			isOpen = true;
		}
	}
	
	/*public ListPage() {
		build();
	}*/
	
	public void build()
	{
		shell = new Shell();
		shell.setSize(623, 650);
		shell.setLocation(150,100);
		shell.setText("InventoryListPage");

		shell.addListener(SWT.Close,  new Listener() {
			public void handleEvent(Event event) {
				isOpen = false;
				shell.setVisible(false);
				shell.dispose();
			}
		});

		setUpTable();
		
		setUpBotton();
			
		loadList();
		
		shell.open();
		
	}
	
	private void setUpTable()
	{
		table = new Table(shell,SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		table.setBounds(40, 40, 400, 500);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		
		final TableColumn id = new TableColumn(table, SWT.NONE);
		id.setWidth(25);
		id.setText("");
		
		final TableColumn itemName = new TableColumn(table, SWT.NONE);
		itemName.setWidth(125);
		itemName.setText("Item Name");
		
		final TableColumn type = new TableColumn(table, SWT.NONE);
		type.setWidth(125);
		type.setText("Type");
		
		final TableColumn price = new TableColumn(table, SWT.NONE);
		price.setWidth(120);
		price.setText("price");
	}
	
	private void setUpBotton()
	{
		create = new Button(shell, SWT.NONE);
		create.setBounds(460, 50, 130, 40);
		create.setText("Create");
		create.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				findCreate();
			}
		});
		
		addInventory = new Button(shell, SWT.NONE);
		addInventory.setBounds(460, 130, 130, 40);
		addInventory.setText("Add Inventory");
		addInventory.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				addInventory();
			}
		});
		
		
		viewItem = new Button(shell, SWT.NONE);
		viewItem.setBounds(460, 210, 130, 40);
		viewItem.setText("View/Modify");
		viewItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				getViews();
				
			}
		});
		
		deleteItem = new Button(shell, SWT.NONE);
		deleteItem.setBounds(460, 290, 130, 40);
		deleteItem.setText("Delete");
		
		deleteItem.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				getCheckedItems();
			}
		});
		
		exit = new Button(shell, SWT.NONE);
		exit.setBounds(460, 366, 130, 40);
		exit.setText("Exit");
		
		exit.addSelectionListener(new SelectionAdapter()
		{
			public void widgetSelected(SelectionEvent e)
			{
				//shell.dispose();
				isOpen = false;
				shell.setVisible(false);
				shell.dispose();
			}
		});
	}
	
	public void loadList()
	{
		AccessDB.loadList(table);
		
		/*table.removeAll();
		DBControllerInterface db = DBHolder.getDb();
		
		db.connect();
		
		for(DBObj dbItem:db.getDBItems())
		{
			if(dbItem != null)
				setRow(dbItem);
		}
		
		db.close();*/
	}
	
	/*private void setRow(DBObj dbObj)
	{
		TableItem row;
		Item rowInfo = (Item)dbObj.getData();
		row = new TableItem(table, SWT.NONE);
		row.setText(new String[] {""+dbObj.getId() , rowInfo.getName() , rowInfo.getType() , ""+rowInfo.getPrice()});
	}*/
	
	private void getCheckedItems()
	{
		ArrayList<DeleteItem> deleteItems = new ArrayList<DeleteItem>();
		
		for(int i = 0; i < table.getItemCount(); i++)
		{
			if(table.getItem(i).getChecked())
			{
				deleteItems.add(new DeleteItem(i, Integer.parseInt(table.getItem(i).getText(0)), table.getItem(i).getText(1)));
			}
		}
		
		if(deleteItems.size() > 0){
			confirmDeleteItems(deleteItems);
		}
		else{
			displayInfoDialog("No items are checked to delete");
		}
	}
	
	private void displayInfoDialog(String msg){
		MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK );
		messageBox.setText("Information"); 
		messageBox.setMessage(msg);
		messageBox.open();
	}
	
	private void confirmDeleteItems(ArrayList<DeleteItem> itemsToDel){
		MessageBox messageBox = new MessageBox(shell,SWT.ICON_QUESTION | SWT.APPLICATION_MODAL | SWT.YES | SWT.NO);
		messageBox.setText("Information"); 
		messageBox.setMessage("Are you sure you want to delete:\n" + getAllDeleteItemNames(itemsToDel) + "\n");
		
		if(messageBox.open() == SWT.YES){
			deleteItems(itemsToDel);
			if (getInventoryTable() != null)
				AccessDB.loadInventoryList(getInventoryTable());
			removeFromTable(itemsToDel);
		}
	}
	
	private String getAllDeleteItemNames(ArrayList<DeleteItem> items){
		String ret = "";
		for(DeleteItem item: items){
			ret += item.getName() + "\n";
		}
		
		return ret;
	}
	
	private void removeFromTable(ArrayList<DeleteItem> items){
		int errorOccurred = 0;
		String errorMsg = "";
		
		for(int i = 0; i < items.size(); i++){
			if(items.get(i).isSuccess()){
				table.remove(items.get(i).getTableInd()-(i - errorOccurred));
			}
			else{
				errorMsg += items.get(i).getName();
				errorOccurred++;
			}
		}
		
		if(errorOccurred > 0){
			displayInfoDialog("Error occurred deleting:\n" + errorMsg);
		}
	}
	
	private void deleteItems(ArrayList<DeleteItem> items){
		AccessDB.deleteItems(items);
	}
	
	private void getViews()
	{
		AccessDB.getViews(this, table);		
	}
		
	private void findCreate()
	{
		int id;
		if ((id = getFirstCheckedItemId()) >= 0) 
		{
			DBObj dbObj = AccessDB.getDBObjById(id);
			new CreateFromExistPage(this, dbObj);
		}
		else
			new CreatePage(this);
		
	}
	private void addInventory()
	{
		AccessDB.addInventory(table, getInventoryTable());
	}

	private Table getInventoryTable() {
		if (parent.inventoryPage != null)
			return parent.inventoryPage.getTable();
		else 
			return null;
	}

	private int getFirstCheckedItemId() {
		int id = -1;
		boolean found = false;
		DBControllerInterface db = DBHolder.getDb();
		
		db.connect();
		for(int i=0;i<table.getItemCount() && !found;i++)
		{
			if (table.getItem(i).getChecked())
			{
				id = db.getDBItems().get(i).getId();
				found = true;
			}
		}
		
		return id;
	}
}

