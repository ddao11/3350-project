package InventoryListPage;

import java.text.DecimalFormat;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import BusinessLogic.SalesCalculator;
import DomainObj.DateRange;
import acceptanceTests.Register;
import acceptanceTests.EventLoop;

import org.eclipse.swt.widgets.Text;

public class SalesPickerDialog extends Dialog {

	protected Object result;
	protected Shell shell;
	protected DateTime datFrom;
	protected DateTime datTo;
	private Combo cboType;
	private Text txtResult;
	private Button cmdCalculate;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public SalesPickerDialog(Shell parent, int style) {
		super(parent, style);
		Register.newWindow(this);
		setText("SWT Dialog");
		
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		if (EventLoop.isEnabled()) {
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), getStyle());
		shell.setSize(371, 341);
		shell.setText(getText());
		shell.setLayout(null);
		
		Composite composite = new Composite(shell, SWT.NONE);
		composite.setBounds(0, 0, 365, 32);
		composite.setLayout(null);
		
		Label lblNewLabel = new Label(composite, SWT.NONE);
		lblNewLabel.setBounds(0, 0, 365, 30);
		lblNewLabel.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.BOLD));
		lblNewLabel.setAlignment(SWT.CENTER);
		lblNewLabel.setText("Get Sales Data");
		
		Label label = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setBounds(0, 30, 365, 2);
		
		Composite composite_1 = new Composite(shell, SWT.NONE);
		composite_1.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.BOLD));
		composite_1.setBounds(0, 32, 365, 311);
		composite_1.setLayout(null);
		
		Label lblCalculate = new Label(composite_1, SWT.NONE);
		lblCalculate.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.NORMAL));
		lblCalculate.setBounds(17, 11, 80, 23);
		lblCalculate.setText("Calculate:");
		
		cboType = new Combo(composite_1, SWT.NONE);
		addSalesDataTypes(cboType);
		cboType.setBounds(103, 11, 91, 23);
		cboType.setText("Select...");
		
		Label lblFrom = new Label(composite_1, SWT.NONE);
		lblFrom.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblFrom.setBounds(28, 93, 44, 23);
		lblFrom.setText("From: ");
		
		datFrom = new DateTime(composite_1, SWT.BORDER);
		datFrom.setDate(2014, 0, 1);
		datFrom.setBounds(78, 92, 80, 24);
		
		Label lblTo = new Label(composite_1, SWT.NONE);
		lblTo.setText("To:");
		lblTo.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblTo.setBounds(226, 93, 27, 23);
		
		datTo = new DateTime(composite_1, SWT.BORDER);
		datTo.setDate(2014, 11, 31);
		datTo.setBounds(259, 93, 80, 24);
		
		Label lblSelectDates = new Label(composite_1, SWT.NONE);
		lblSelectDates.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.NORMAL));
		lblSelectDates.setBounds(17, 52, 151, 25);
		lblSelectDates.setText("Select Dates");
		
		Label lblSelectDates_1 = new Label(composite_1, SWT.SEPARATOR | SWT.HORIZONTAL);
		lblSelectDates_1.setText("Select Dates");
		lblSelectDates_1.setBounds(17, 75, 110, 12);
		
		cmdCalculate = new Button(composite_1, SWT.NONE);
		cmdCalculate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String type = cboType.getText();
				SalesCalculator sp = new SalesCalculator();
				double value = 0;
				String strValue;
				boolean valid = false;
				
				if (type.equals("Total Sales")) {
					value = sp.getTotalSales(new DateRange(getDateString(datFrom), getDateString(datTo)));
					valid = true;
				}
				else if (type.equals("Etsy Fees")) {
					value = sp.getEtsyFees(new DateRange(getDateString(datFrom), getDateString(datTo)));
					valid = true;
				}
				else {
					txtResult.setText("Please choose a type.");
				}
				
				if (valid) {
					strValue = (new DecimalFormat("0.00")).format(value);
					txtResult.setText(String.format("$%s", strValue));
				}
			}

			private String getDateString(DateTime datFrom) {
				return datFrom.getYear() + "-" +
						String.format("%02d", datFrom.getMonth()) + "-" +
						String.format("%02d", datFrom.getDay());
						
			}
		});
		cmdCalculate.setBounds(133, 152, 96, 32);
		cmdCalculate.setText("Calculate");
		
		txtResult = new Text(composite_1, SWT.BORDER | SWT.CENTER);
		txtResult.setFont(SWTResourceManager.getFont("Segoe UI", 16, SWT.BOLD));
		txtResult.setBounds(28, 221, 303, 32);
		
		

	}
	
	private void addSalesDataTypes(Combo c) {
		c.setItems(new String[] {"Total Sales", "Etsy Fees"});
	}
}
