package InventoryListPage;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import BusinessLogic.AccessDB;
import Database.DBControllerInterface;
import Database.DBHolder;
import DomainObj.Item;
import acceptanceTests.Register;

public class CreatePage extends MasterPage {
	
	private Button create;
	
	public CreatePage(ListPage parent)
	{
		Register.newWindow(this);
        display = Display.getDefault();
        createWindow(parent);
	}
	
	public void createWindow(final ListPage parent)
	{
		shell = new Shell();
		shell.setSize(500, 615);
		shell.setLocation(200,125);
		shell.setText("Create Page");
		
		super.createWindow();
		
		create = new Button(shell, SWT.NONE);
		create.setBounds(130, 500, 70, 30);
		create.setText("create");
		create.addSelectionListener(new SelectionAdapter()
 		{
			public void widgetSelected(SelectionEvent e)
			{
				getMaterials();
				if(isValid())
				{
					AccessDB.addItem(new Item(nameText.getText(), type.getText(), material, Double.parseDouble(price.getText())));
					
					parent.loadList();
						
					shell.dispose();
				}
				else
				{
					displayInvalid();
				}
			}
		});
		shell.open();
	}
	
	
}
