package InventoryListPage;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import BusinessLogic.AccessDB;
import Database.DBControllerInterface;
import Database.DBHolder;
import Database.DBObj;
import DomainObj.Item;
import acceptanceTests.Register;

public class CreateFromExistPage extends MasterPage {

		private Button create;
		
		public CreateFromExistPage(final ListPage parent,DBObj dbObj)
		{
			Item item = (Item)dbObj.getData();
			display = Display.getDefault();
	        createWindow();
	       
	        nameText.setText(item.getName());
			type.setText(item.getType());
			price.setText(""+item.getPrice());
			
			material = item.getMaterials();
			
			setMaterials();
			
			create.addSelectionListener(new SelectionAdapter()
	 		{
				public void widgetSelected(SelectionEvent e)
				{	
					getMaterials();
					if(isValid())
					{
						AccessDB.addItem(new Item(nameText.getText(), type.getText(), material, Double.parseDouble(price.getText())));
						
						shell.dispose();
						parent.loadList();
					}
					else
					{
						displayInvalid();
					}
					
				}
			});
			
	        shell.open();
		}
		
		public void createWindow()
		{
			shell = new Shell();
			shell.setSize(500, 615);
			shell.setLocation(200,125);
			shell.setText("Create from existing Items");
			
			super.createWindow();
			
			create = new Button(shell, SWT.NONE);
			create.setBounds(130, 500, 70, 30);
			create.setText("Create");
			
		}
		
}
