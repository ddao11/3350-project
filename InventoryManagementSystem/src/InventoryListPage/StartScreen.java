package InventoryListPage;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import acceptanceTests.Register;
import acceptanceTests.EventLoop;

public class StartScreen {

	protected Shell shell;
	private Button cmdViewItems;
	private Button cmdViewInventory;
	private Button cmdCalculate;

	public static ListPage itemPage;
	public static InventoryListPage inventoryPage;
	
	public StartScreen() {
		Register.newWindow(this);
	}
	
	/**
	 * Launch the application.
	 * @param args
	 *//*
	public static void main(String[] args) {
		try {
			StartScreen window = new StartScreen();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		if (EventLoop.isEnabled()) {
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(450, 243);
		shell.setText("Inventory Management System");
		
		cmdViewItems = new Button(shell, SWT.NONE);
		final StartScreen startScreen = this;
		cmdViewItems.addSelectionListener(new SelectionAdapter() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				itemPage = new ListPage(startScreen);
			}
		});
		cmdViewItems.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.NORMAL));
		cmdViewItems.setBounds(102, 37, 230, 40);
		cmdViewItems.setText("View Items");
		
		cmdViewInventory = new Button(shell, SWT.NONE);
		cmdViewInventory.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				inventoryPage = new InventoryListPage();
				//inventoryPage.build();
			}
		});
		cmdViewInventory.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.NORMAL));
		cmdViewInventory.setBounds(102, 79, 230, 40);
		cmdViewInventory.setText("View Inventory");
		
		cmdCalculate = new Button(shell, SWT.NONE);
		cmdCalculate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				SalesPickerDialog sp = new SalesPickerDialog(shell, SWT.DIALOG_TRIM);
				sp.open();
			}
		});
		cmdCalculate.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.NORMAL));
		cmdCalculate.setBounds(102, 124, 230, 40);
		cmdCalculate.setText("Calculate Sales Data");

	}

}
