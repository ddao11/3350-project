package BusinessLogic;

import java.util.ArrayList;

import Database.DBControllerInterface;
import Database.DBHolder;
import DomainObj.DateRange;
import DomainObj.Inventory;
import DomainObj.Status;

public class SalesCalculator {
	
	private static double ETSY_PERCENTAGE = 0.02;
	//private ArrayList<InventoryItem> inventory = new ArrayList<InventoryItem>();
	
	//private DBControllerInterface db = DBHolder.getDb(); // This uses stub for now
		
	public double getEtsyFees(DateRange range) {
		DBControllerInterface db = DBHolder.getDb();
		db.connect();
		ArrayList<Inventory> inventory = db.getInventoryItems(Status.SoldOnEtsy, range);
		double totalSold = 0;
		
		
		for(Inventory inv : inventory) {
			totalSold += inv.getStatusPrice();
		}
		db.close();
		return totalSold * ETSY_PERCENTAGE;
	}
	
	public double getTotalSales(DateRange range) {
		DBControllerInterface db = DBHolder.getDb();
		db.connect();
		ArrayList<Inventory> inventory = db.getInventoryItems(Status.SoldOnEtsy, range);
		double totalSold = 0;
		
		// Sales include etsy sales and other sales
		inventory = db.getInventoryItems(Status.SoldOnEtsy, range);
		inventory.addAll(db.getInventoryItems(Status.SoldOther, range));
		db.close();
		
		for(Inventory inv : inventory) {
			totalSold += inv.getStatusPrice();
		}
		
		return totalSold;
	}
	
}
