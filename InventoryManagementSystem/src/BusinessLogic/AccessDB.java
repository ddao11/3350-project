package BusinessLogic;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import Database.DBControllerInterface;
import Database.DBHolder;
import Database.DBObj;
import DomainObj.DeleteItem;
import DomainObj.Inventory;
import DomainObj.Item;
import DomainObj.Material;
import InventoryListPage.ListPage;
import InventoryListPage.ViewPage;

public class AccessDB {
	private static DBControllerInterface db = DBHolder.getDb();
	
	public static int addItem(Item item) {
		db.connect();
		int itemId = db.addItem(item);
		db.close();
		return itemId;
	}
	
	public static void loadInventoryList(Table table) {
		table.removeAll();
		
		db.connect();
		
		for(DBObj dbInventoryItem:db.getDBInventoryItems())
		{
			for(DBObj dbItem:db.getDBItems()){
				if(((Inventory)dbInventoryItem.getData()).getID()==dbItem.getId()){
					setRow((Inventory)dbInventoryItem.getData(),(Item)dbItem.getData(), table);
				}
			}
		}
		
		db.close();
	}
	
	public static void loadList(Table table) {
		table.removeAll();
		
		db.connect();
		
		for(DBObj dbItem:db.getDBItems())
		{
			if(dbItem != null)
				setRow(dbItem, table);
		}
		
		db.close();
	}
	
	public static void loadByList(ArrayList<Inventory> list, Table table) {
		table.removeAll();
		db.connect();
		if(list!=null)
		for(int i=0;i<list.size();i++)
		{
			for(DBObj dbItem2:db.getDBItems()){
				Inventory inventory=list.get(i);
				Item item=(Item)dbItem2.getData();
				if(inventory.getID()==dbItem2.getId()){
					setRow(inventory,item, table);
				}
			}
		}
		db.close();
	}
	
	public static void deleteInventoryItems(Table table) {
		db.connect();

		for(int i = 0; i < table.getItemCount(); i++)
		{
			if(table.getItem(i).getChecked())
			{
				db.deleteInventoryItem(Integer.parseInt(table.getItem(i).getText(0)));
			}
		}
		
		loadInventoryList(table);
		db.close();
	}
	
	public static void changeStatus(Table table, String status) {
		db.connect();		
		for(int i = 0; i < table.getItemCount(); i++)
		{
			if(table.getItem(i).getChecked())
			{
				Inventory inv =	(Inventory)	(db.getInventoryItemById(Integer.parseInt(table.getItem(i).getText(0))).getData());//.setStatus(status); //has to change this in the db
				inv.setStatus(status);
				db.updateInventory(inv);
				
			}
		}
		
		loadInventoryList(table);
		db.close();
	}
	
	public static void deleteItems(ArrayList<DeleteItem> items) {
		db.connect();
		
		for(int i = 0; i < items.size(); i++){
			items.get(i).setSuccess(db.deleteItem(items.get(i).getDbId()));
		}
		
		db.close();
	}
	
	public static void getViews(ListPage parent, Table table) {
		for(int i=0;i<table.getItemCount();i++)
		{
			if(table.getItem(i).getChecked())
			{
				db.connect();
				new ViewPage(parent,db.getDBItems().get(i));
				db.close();
			}
		}
	}

	public static void addInventory(Table table, Table inventoryTable)
	{
		db.connect();
		for(int i = 0; i < table.getItemCount(); i++)
		{
			if(table.getItem(i).getChecked())
			{
				db.addInventoryItem(Integer.parseInt(table.getItem(i).getText(0)),"Available");
			}
		}
		if (inventoryTable != null)
			loadInventoryList(inventoryTable);
		
		db.close();
	}

	public static ArrayList<Material> getAllMaterials() {
		db.connect();
		ArrayList<Material>	material = db.getAllMaterials();
		db.close();
		return material;
	}
	
	public static void updateItem(int id, Item item) {
		db.connect();
		
		DBObj dbObj = db.getDbObj(id);

		dbObj.setData(item);
		db.updateItem(dbObj);
		
		db.close();
	}

	public static DBObj getDBObjById(int id) {
		db.connect();
		DBObj dbObj = db.getDBItems().get(id);
		db.close();
		return dbObj;
	}
	/****************************************************/
	private static void setRow(Inventory inventoryInfo,Item itemInfo, Table table)
	{
		TableItem row;

		row = new TableItem(table, SWT.NONE);
		row.setText(new String[] {""+inventoryInfo.getInventoryID() , itemInfo.getName() , inventoryInfo.getStatus() , ""});
	}
	
	private static void setRow(DBObj dbObj, Table table)
	{
		TableItem row;
		Item rowInfo = (Item)dbObj.getData();
		row = new TableItem(table, SWT.NONE);
		row.setText(new String[] {""+dbObj.getId() , rowInfo.getName() , rowInfo.getType() , ""+rowInfo.getPrice()});
	}
	
	public static void setDb(DBControllerInterface otherDb) {
		db = otherDb;
	}

}
