package BusinessLogic;

import java.util.ArrayList;

import org.eclipse.swt.widgets.Shell;

import Database.DBControllerInterface;
import Database.DBHolder;
import DomainObj.Inventory;

public class Search {
	
	protected Object data;
	protected DBControllerInterface db;
	//protected Inventory inventoryObj;
	protected Shell shell;
	private ArrayList<Inventory> resultListID;
	private ArrayList<Inventory> resultListStatus;
	
	public Search(){
		data=null;
		//this.inventoryObj=dbObj;
	}
	
	public boolean searchById(int id){
		boolean flag=false;
		db = DBHolder.getDb();
		db.connect();
		//go to database to search id
		resultListID=getInventoryItemByID(id);
		if(resultListID!=null)
		{
			flag=true;
			//new InventoryListPage(resultListID);
		}
		return flag;
	}
	
	public boolean searchByStatus(String status){
		boolean flag=false;
		db = DBHolder.getDb();
		db.connect();
		//go to data base to search type
		resultListStatus=getInventoryItemByStatus(status);
		if(resultListStatus!=null)
		{
			flag=true;
			//new InventoryListPage(resultListStatus);
		}
		return flag;
	}
	
	public ArrayList<Inventory> getInventoryItemByStatus(String status){
		db = DBHolder.getDb();
		db.connect();
		resultListStatus=new ArrayList<Inventory>();
		ArrayList<Inventory> dbArrayList=db.getInventory();
		for(int i = 0; i < dbArrayList.size(); i++){
			if(dbArrayList.get(i).getStatus().equals(status)){
				resultListStatus.add(dbArrayList.get(i));
			}
		}
		db.close();
		return resultListStatus;
	}
	
	public ArrayList<Inventory> getInventoryItemByID(int id){
		db = DBHolder.getDb();
		db.connect();
		if(db.isConnected()){
			resultListID=new ArrayList<Inventory>();
			ArrayList<Inventory> dbArrayList=db.getInventory();
			if(dbArrayList!=null)
			for(int i = 0; i < dbArrayList.size(); i++){
				if(dbArrayList.get(i).getInventoryID()==id){
					resultListID.add(dbArrayList.get(i));
					
				}
			}
		}
		db.close();
		return resultListID;
	}
}
