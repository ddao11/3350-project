package DomainObj;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Item {
	private String name;
	private String type;
	private ArrayList<Material> materials;
	private double price;
	
	public Item(){
		name = "";
		type = "";
		materials = new ArrayList<Material>();
		price = 0;
	}
	
	public Item(String name, String type, ArrayList<Material> materials, double price){
		setName(name);
		setType(type);
		setMaterials(materials);
		setPrice(price);
	}
	
	public String toString(){
		return name + " " + type + " " + materials + " " + price;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setType(String type){
		this.type = type;
	}
	
	public void setMaterials(ArrayList<Material> materials){
		this.materials = materials;
	}
	
	public void setPrice(double price){
		DecimalFormat f = new DecimalFormat("##.00");
		this.price = Double.parseDouble(f.format(price));
	}
	
	public String getName(){
		return name;
	}
	
	public String getType(){
		return type;
	}
	
	public ArrayList<Material> getMaterials(){
		return materials;
	}
	
	public double getPrice(){
		return price;
	}
}
