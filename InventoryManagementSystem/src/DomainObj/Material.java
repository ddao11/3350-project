package DomainObj;

import java.text.DecimalFormat;

public class Material {
	private String name;
	private double cost;
	
	public Material(){
		name = "";
		cost = 0;
	}
	
	public Material(String name, double cost){
		setName(name);
		setCost(cost);
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public void setCost(double cost){
		DecimalFormat f = new DecimalFormat("##.00");
		this.cost = Double.parseDouble(f.format(cost));
	}
	
	public double getCost(){
		return cost;
	}
	
	public String toString(){
		
		return name + " " + cost;
	}
	
	public boolean equals(Material material){
		return (this.getName().equals(material.getName()) && this.getCost() == material.getCost());
	}
}
