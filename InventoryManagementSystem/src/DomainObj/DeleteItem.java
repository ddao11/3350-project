package DomainObj;

public class DeleteItem {
	private int tableInd;
	private int dbId;
	private boolean success;
	private String name;
	
	public DeleteItem(int tableInd, int dbId, boolean success, String name){
		setTableInd(tableInd);
		setDbId(dbId);
		setSuccess(success);
		setName(name);
	}
	
	public DeleteItem(int tableInd, int dbId, String name){
		setTableInd(tableInd);
		setDbId(dbId);
		setName(name);
		success = false;
	}
	
	public int getTableInd() {
		return tableInd;
	}

	public void setTableInd(int tableInd) {
		this.tableInd = tableInd;
	}

	public int getDbId() {
		return dbId;
	}

	public void setDbId(int dbId) {
		this.dbId = dbId;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
