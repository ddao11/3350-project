package DomainObj;

public enum Status {
	SoldOnEtsy("SoldOnEtsy"),
	SoldOther("SoldOther"),
	Lost("Lost"),
	Damaged("Damaged"),
	Donated("Donated"),
	Available("Available"),
	Uninitialized("");
	
	private final String value;
	
	private Status(String s) {
		value = s;
	}
	
	@Override
	public String toString() {
		return value;
	}
}
