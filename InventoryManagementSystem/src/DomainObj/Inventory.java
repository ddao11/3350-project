package DomainObj;

import Database.DBControllerInterface;
import Database.DBHolder;
import Database.DBObj;

//import java.text.DecimalFormat;

public class Inventory {
	private int itemID;
	private Status status;
	private int inventoryID;
	private String statusDate;   // required for Calculating Etsy Fees & Sales Data
	private double statusPrice;  // required for Calculating Etsy Fees & Sales Data

	
	public Inventory(){
		itemID = 0;
		status = Status.Uninitialized;
	}
	
	public Inventory(int itemID, String status){
		setItemID(itemID);
		setStatus(status);
	}
	
	public Inventory(int itemID, String status, int id){
		setItemID(itemID);
		setStatus(status);
		setInventoryID(id);
	}
	
	public Inventory(int itemID, Status status, int id, String date, double price) {
		setItemID(itemID);
		this.status = status;
		setInventoryID(id);
		setStatusDate(date);
		setStatusPrice(price);
	}
	
	public String toString(){
		return itemID + " " + status ;
	}
	
	public void setItemID(int itemID){
		this.itemID = itemID;
	}
	
	public void setStatus(String status){
		this.status = Status.valueOf(status.replaceAll(" ", ""));
	}
	public void setInventoryID(int inventoryID){
		this.inventoryID = inventoryID;
	}

	public int getID(){
		return itemID;
	}
	
	public String getStatus(){
		return status.toString();
	}
	
	public int getInventoryID(){
		return inventoryID;
	}

	public String getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(String statusDate) {
		this.statusDate = statusDate;
	}

	public double getStatusPrice() {
		return statusPrice;
	}

	public void setStatusPrice(double statusPrice) {
		this.statusPrice = statusPrice;
	}
	
	public String getName(){
		DBControllerInterface db = DBHolder.getDb();
		Item info;
		String result="";
		db.connect();
		
		for(DBObj dbItem:db.getDBItems())
		{
			if(dbItem != null){
				if(dbItem.getId()==itemID){
					info = (Item)dbItem.getData();
					result = info.getName();
				}
			}

		}
		return result;
	}
	
}
