package Tests.DomainObjTests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import DomainObj.Material;

public class MaterialTest {

	Material material;
	
	@Before
	public void setUp() throws Exception {
		material = new Material("Paper", 5.00);
	}

	@Test
	public void testSetName() {
		material.setName("Leather");
		assertTrue("Should be 'Leather'", material.getName().equals("Leather"));
	}

	@Test
	public void testSetCost() {
		material.setCost(20.00);
		assertTrue("Should be 20.00", material.getCost() == 20.00);
	}

	@Test
	public void testEqualsMaterial() {
		Material a = new Material("Book", 20.00);
		Material b = new Material("Book", 20.00);
		assertTrue(a.equals(b));
	}

	@Test
	public void testEqualsMaterial2() {
		Material a = new Material("Book", 20.00);
		Material b = new Material("Card", 20.00);
		assertFalse(a.equals(b));
	}
	
	@Test
	public void testEqualsMaterial3() {
		Material a = new Material("Book", 20.00);
		Material b = new Material("Book", 19.00);
		assertFalse(a.equals(b));
	}
	
	@Test
	public void testEqualsMaterial5() {
		Material a = new Material("Book", 20.00);
		Material b = new Material(null, 20.00);
		assertFalse(a.equals(b));
	}
	
	
}
