package Tests.DomainObjTests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import DomainObj.DateRange;

public class DateRangeTest {
	DateRange range = new DateRange("2014-07-11", "2014-07-20");
	
	
	@Test
	public void GetFromTest() {
		assertTrue("Should be 2014-07-11", range.getFrom().equals("2014-07-11"));
	}
	
	@Test
	public void GetToTest() {
		assertTrue("Should be 2014-07-20", range.getTo().equals("2014-07-20"));
	}
	
	@Test
	public void SetToTest() {
		range.setTo("2014-12-25");
		assertTrue("Should be 2014-12-25", range.getTo().equals("2014-12-25"));
		range.setTo("2014-07-11");
	}

	@Test
	public void SetFromTest() {
		range.setFrom("2014-12-25");
		assertTrue("Should be 2014-12-25", range.getFrom().equals("2014-12-25"));
		range.setFrom("2014-07-20");
	}
}
