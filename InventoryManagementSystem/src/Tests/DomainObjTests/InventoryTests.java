package Tests.DomainObjTests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import DomainObj.Inventory;
import DomainObj.Status;

public class InventoryTests {

	private Inventory inv;
	Status status;
	
	@Before
	public void setUp() throws Exception {
		status = Status.Available;
		inv = new Inventory(1, status, 2, "2014-07-11", 50.00);
	}

	@Test
	public void testSetStatus() {
		inv.setStatus("Lost");
		assertTrue("Should be lost", inv.getStatus().equals("Lost"));
	}

	@Test
	public void testSetInventoryID() {
		inv.setInventoryID(101);
		assertTrue("Should be 101", inv.getInventoryID() == 101);
	}

	@Test
	public void testSetStatusDate() {
		inv.setStatusDate("2014-03-21");
		assertTrue("Should be 2014-03-21", inv.getStatusDate().equals("2014-03-21"));
	}

	@Test
	public void testSetStatusPrice() {
		inv.setStatusPrice(20.00);
		assertTrue("Should be 20.00", inv.getStatusPrice() == 20);
	}

}
