package Tests.DomainObjTests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import DomainObj.Status;

public class StatusTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testToString() {
		Status s = Status.Available;
		assertTrue(s.toString().equals("Available"));
	}

}
