package Tests.DomainObjTests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import DomainObj.Item;
import DomainObj.Material;

public class ItemTest {

	private Item item;
	private ArrayList<Material> material;
	
	@Before
	public void setUp() throws Exception {
		material = new ArrayList<Material>();
		material.add(new Material("Leather", 10.00));
		material.add(new Material("SketchPaper", 5.00));
		item = new Item("Red Leather Book", "Journal", material, 50.00);
	}

	@Test
	public void testSetName() {
		item.setName("Brown Book");
		assertTrue("Should be 'Brown Book'", item.getName().equals("Brown Book"));
	}

	@Test
	public void testSetType() {
		item.setType("Book");
		assertTrue("Should be 'Book'", item.getType().equals("Book"));
	}

	@Test
	public void testSetMaterials() {
		ArrayList<Material> newMat = new ArrayList<Material>();
		newMat.add(new Material("Thread", 2.00));
		item.setMaterials(newMat);
		assertTrue("Material should be only Thread", item.getMaterials() == newMat);
	}

	@Test
	public void testSetPrice() {
		item.setPrice(100.00);
		assertTrue("Should be 100", item.getPrice() == 100.00);
	}

}
