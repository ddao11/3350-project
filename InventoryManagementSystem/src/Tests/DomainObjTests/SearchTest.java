package Tests.DomainObjTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import junit.framework.TestSuite;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Database.DBControllerInterface;
import Database.DBControllerStub;
import Database.DBHolder;
import Database.DBObj;
import Database.Search;
import DomainObj.Inventory;
import DomainObj.Item;
import DomainObj.Material;

public class SearchTest extends TestSuite {
	Search instance;
	DBControllerInterface db;
	ArrayList<Material> material;
	Item testItem;
	
	@Before
	public void setUp() throws Exception {
		db = DBHolder.getDb();
		material = new ArrayList<Material>();
		instance = new Search();	
	}

	@After
	public void tearDown() throws Exception {
	}

	//********************************************
	//			Helper Methods
	//********************************************

	
	//********************************************
	//			Connection Tests
	//********************************************
	
	
	@Test
	public void searchByStatus() {
		db.connect();
		ArrayList<Inventory> testArrayList;
		Inventory testItem;
		testArrayList = instance.getItemByStatus(db.getInventory(),"Available");
		testItem = testArrayList.get(0);
		
		assertEquals("Available", testItem.getStatus());
		assertEquals(1, testItem.getID());
		db.close();
	}
	
	@Test
	public void testSearchById() {
		db.close();
		ArrayList<Inventory> testArrayList;
		Inventory testItem;
		testArrayList = instance.getItemByID(db.getInventory(),"2");
		testItem = testArrayList.get(0);
		
		assertEquals("Lost", testItem.getStatus());
		assertEquals(2, testItem.getID());
	}
	
	@Test
	public void testSearchByName() {
		db.close();
		ArrayList<Inventory> testArrayList;
		Inventory testItem;
		testArrayList = instance.getItemByName(db.getInventory(),"Journal");
		testItem = testArrayList.get(0);
		
		assertEquals("Journal", testItem.getName());
		assertEquals(0, testItem.getID());
	}

	@Test
	public void testSearchByInput() {
		db.close();
		ArrayList<Inventory> testArrayList;
		Inventory testItem;
		testArrayList = instance.getItemByInput("","Journal","Lost");
		testItem = testArrayList.get(0);
		
		assertEquals("Journal", testItem.getName());
		assertEquals(2, testItem.getID());
	}

	@Test
	public void testSearchByInput2() {
		db.close();
		ArrayList<Inventory> testArrayList;
		Inventory testItem;
		testArrayList = instance.getItemByInput("2","","Lost");
		testItem = testArrayList.get(0);
		
		assertEquals("Journal", testItem.getName());
		assertEquals(2, testItem.getID());
	}
	
}
