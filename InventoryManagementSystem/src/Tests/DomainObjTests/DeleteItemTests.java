package Tests.DomainObjTests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import DomainObj.DeleteItem;

public class DeleteItemTests {

	DeleteItem dI = new DeleteItem(1, 2, true, "Book");

	@Test
	public void testGetTableInd() {
		assertTrue("Should be 1", dI.getTableInd() == 1);
	}

	@Test
	public void testSetTableInd() {
		dI.setTableInd(5);
		assertTrue("Should be 5", dI.getTableInd() == 5);
		dI.setTableInd(1);
	}

	@Test
	public void testGetDbId() {
		assertTrue("Should be 2", dI.getDbId() == 2);
	}

	@Test
	public void testSetDbId() {
		dI.setDbId(5);
		assertTrue("Should be 5", dI.getDbId() == 5);
		dI.setDbId(1);
	}

	@Test
	public void testIsSuccess() {
		assertTrue("Should be True", dI.isSuccess());
	}

	@Test
	public void testSetSuccess() {
		dI.setSuccess(false);
		assertTrue("Should be False", !dI.isSuccess());
		dI.setSuccess(true);
	}

	@Test
	public void testGetName() {
		assertTrue("Should be 'Book'", dI.getName().equals("Book"));
	}

	@Test
	public void testSetName() {
		dI.setName("Card");
		assertTrue("Should be 'Card'", dI.getName().equals("Card"));
		dI.setName("Book");
	}

}
