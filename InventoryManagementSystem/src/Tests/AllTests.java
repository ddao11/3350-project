package Tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import Tests.BusinessLogicTests.SalesCalculatorTest;
import Tests.BusinessLogicTests.SearchTest;
import Tests.DatabaseTests.DBControllerFinalTest;
import Tests.DatabaseTests.DBControllerStubTest;
import Tests.DatabaseTests.DBInventoryListTest;
import Tests.DatabaseTests.DBStubObjTest;
import Tests.DomainObjTests.DateRangeTest;
import Tests.DomainObjTests.DeleteItemTests;
import Tests.DomainObjTests.InventoryTests;
import Tests.DomainObjTests.ItemTest;
import Tests.DomainObjTests.MaterialTest;
import Tests.DomainObjTests.StatusTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	DBControllerStubTest.class,
	DBControllerFinalTest.class,
	DBInventoryListTest.class,
	DBStubObjTest.class,
	SalesCalculatorTest.class,
	DateRangeTest.class,
	DeleteItemTests.class,
	InventoryTests.class,
	ItemTest.class,
	MaterialTest.class,
	StatusTest.class
})
public class AllTests {
 
}
