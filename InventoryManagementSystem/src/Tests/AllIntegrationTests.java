package Tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import Tests.IntegrationTests.AccessDBFinalTests;
import Tests.IntegrationTests.AccessDBStubTests;
  
@RunWith(Suite.class)
@SuiteClasses({
	AccessDBStubTests.class,
	AccessDBFinalTests.class
})
public class AllIntegrationTests {

}
