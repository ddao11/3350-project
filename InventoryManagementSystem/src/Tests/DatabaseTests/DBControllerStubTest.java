package Tests.DatabaseTests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import junit.framework.TestSuite;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Database.DBControllerStub;
import DomainObj.Item;
import DomainObj.Material;

public class DBControllerStubTest extends TestSuite {

	DBControllerStub db;
	ArrayList<Material> material;
	Item testItem;
	
	@Before
	public void setUp() throws Exception {
		db = new DBControllerStub();
		material = new ArrayList<Material>();
		testItem = createItem();
	}

	@After
	public void tearDown() throws Exception {
	}

	//********************************************
	//			Helper Methods
	//********************************************
	private Item createItem() {
		ArrayList<Material> tempMaterial = new ArrayList<Material>();
		tempMaterial.add(new Material("Leather", 10.00));
		return new Item("Leather Book", "Book", tempMaterial, 65.00);
	}
	
	private boolean detailsMatch(Item a, Item b) {
		return a.getMaterials() == b.getMaterials() &&
				a.getName() == b.getName() &&
				a.getType() == b.getType() &&
				a.getPrice() == b.getPrice();
	}
	
	//********************************************
	//			Connection Tests
	//********************************************
	
	@Test
	public void testConnect() {
		db.connect();
		assertTrue("DB should be connected", db.isConnected());
		db.close();
	}
	
	@Test
	public void testClose() {
		db.close();
		assertTrue("DB should be disconnected", !db.isConnected());
	}
			
	//********************************************
	//			Insertion Tests
	//********************************************
	
	@Test
	public void testInsertItemWithoutConnecting() {
		db.close();
		int addedItem = db.addItem(testItem);
		assertTrue("Item should not be in DB", db.getItem(addedItem) == null);
	}
	
	@Test
	public void testInsertDetailsWithoutConnect() {
		db.close();		
		int addedItem = db.addItem("Leather Book","Book",material,20.0);
		assertTrue("Item should not be in DB", db.getItem(addedItem) == null);
	}
	
	@Test
	public void testInsertNull() {
		db.connect();
		Item item = new Item(null, null, null, 0);
		int addedItem = db.addItem(item);
		assertTrue("Item should exist", db.getItem(addedItem) == item);
		db.close();
	}
	
	@Test
	public void testInsertEmptyStrings() {
		db.connect();
		Item item = new Item("", "", material, 0);
		int addedItem = db.addItem(item);
		assertTrue("Item should exist", db.getItem(addedItem) == item);
		db.close();
	}
	
	@Test 
	public void testInsertMaxPrice() {
		db.connect();
		Item item = new Item("Leather Book", "Book", material, Double.MAX_VALUE);
		int addedItem = db.addItem(item);
		assertTrue("Item should exist", db.getItem(addedItem).getPrice() == Double.MAX_VALUE);
		db.close();
	}
	
	@Test 
	public void testInsertNegativePrice() {
		// Check for non-errors - maybe we should set to zero if negative value entered?
		db.connect();
		Item item = new Item("Leather Book", "Book", material, -1);
		int addedItem = db.addItem(item);
		assertTrue("Item should exist", detailsMatch(item, db.getItem(addedItem)));
		db.close();
	}
	
	@Test
	public void testInsertOneItem() {
		db.connect();
		int addedItem = db.addItem(testItem);
		assertTrue("Item should be in DB", db.getItem(addedItem) == testItem);
		db.close();
	}
	
	@Test
	public void testInsertOneItemByDetails() {
		db.connect();
		int addedItem = db.addItem("Leather Book", "Book", material, 10.0);
		Item item = db.getItem(addedItem);
		assertTrue("Item should be in DB",
				item != null &&
				item.getMaterials() == material &&
				item.getName() == "Leather Book" &&
				item.getType() == "Book" &&
				item.getPrice() == 10.0);
		db.close();
	}
	
	@Test
	public void testInsertStress() {
		db.connect();
		int numItems = 1000;
		boolean testPassed = true;
		int cc = 0;
		int [] items = new int[numItems];
		
		
		
		for (int i = 0; i < numItems; i++) {
			items[i] = db.addItem(createItem());
		}
		
		while (testPassed && cc < numItems) {
			if (db.getItem(items[cc]) == null) {
				testPassed = false;
			}
			
			cc++;
		}
		
		assertTrue("Stress test didn't succeed.", testPassed);
	}
	
	//********************************************
	//			Delete Tests
	//********************************************
	
	@Test
	public void testDeleteItemWithoutConnecting() {
		db.connect();
		int addedItem = db.addItem(testItem);
		db.close();
		assertTrue("Item should not be deleted", !db.deleteItem(addedItem));
	}
	
	@Test
	public void testDeleteItemById() {
		db.connect();
		int addedItem = db.addItem(testItem);
		db.deleteItem(addedItem);
		assertTrue("Item should not exist", db.getItem(addedItem) == null);
		db.close();
	}
	
	@Test 
	public void testDeleteItem() {
		db.connect();
		int addedItem = db.addItem(testItem);
		db.deleteItem(testItem);
		assertTrue("Item should not exist", db.getItem(addedItem) == null);
		db.close();
	}
	
	@Test
	public void testDeleteNullItem() {
		db.connect();
		int addedItem = db.addItem(null);
		db.deleteItem(addedItem);
		assertTrue("Item should not exists", db.getItem(addedItem) == null);
		db.close();
	}
	
	@Test
	public void testStressDelete() {
		int numItems = 1000;
		int [] id = new int[numItems];
		int oldSize = db.getItems().size();
		
		db.connect();
		
		// add & remove one million items
		for (int i = 0; i < numItems; i++) {
			id[i] = db.addItem(createItem());
		}
		for (int i = 0; i < numItems; i++) {
			db.deleteItem(id[i]);
		}
		assertTrue("Database size error after Stress Delete", db.getItems().size() == oldSize);
		db.close();
		
		
	}
	
}
