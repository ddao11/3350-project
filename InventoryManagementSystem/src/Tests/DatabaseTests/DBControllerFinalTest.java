package Tests.DatabaseTests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import Database.DBControllerFinal;
//import Database.DBControllerStub;
import DomainObj.Item;
import DomainObj.Material;

public class DBControllerFinalTest {

	DBControllerFinal db;
	ArrayList<Material> material;
	Item testItem;
	
	@Before
	public void setUp() throws Exception {
		db = new DBControllerFinal();
		material = new ArrayList<Material>();
		testItem = createItem();
	}

	//********************************************
	//			Helper Methods
	//********************************************
	private Item createItem() {
		//ArrayList<Material> tempMaterial = new ArrayList<Material>();
		//tempMaterial.add(new Material("Leather", 10.00));
		return new Item("Leather Book", "Book", null, 65.00);
	}
	
	private boolean materialsEqual(ArrayList<Material> m1, ArrayList<Material> m2) {
		boolean equal = true;
		int m1Size = (m1 == null) ? 0 : m1.size();
		int m2Size = (m2 == null) ? 0 : m2.size();
		
		if (m1Size != m2Size) {
			equal = false;
		}
		else if (m1Size == 0 && m2Size == 0) {
			equal = true;
		}
		else {
			for (int i = 0; i < m1.size() && equal; i++) {
				if (!m1.get(i).equals(m2.get(i))) {
					equal = false;
				}
			}
		}
		
		return equal;
	}
	
	private String isNull(String value, String nullValue) {
		return (value != null) ? value : nullValue;
	}
	
	private boolean itemsEqual(Item i1, Item i2) {
		//boolean equal = true;
		String i1Name = isNull(i1.getName(), "null");
		String i2Name = isNull(i2.getName(), "null");
		String i1Type = isNull(i1.getType(), "null");
		String i2Type = isNull(i2.getType(), "null");
		
		return (
				i1Name.equals(i2Name) &&
				i1Type.equals(i2Type) &&
				materialsEqual(i1.getMaterials(), i2.getMaterials()) &&
				i1.getPrice() == i2.getPrice()
		);
						
				
						
	}
	
	//********************************************
	//			Connection Tests
	//********************************************
	
	@Test
	public void testConnect() {
		db.connect();
		assertTrue("DB should be connected", db.isConnected());
		db.close();
	}
	
	@Test
	public void testClose() {
		db.connect();
		db.close();
		assertTrue("DB should be disconnected", !db.isConnected());
	}
			
	//********************************************
	//			Insertion Tests
	//********************************************
	
	@Test
	public void testInsertItemWithoutConnecting() {
		db.connect();
		db.close();
		
		int addedItem = db.addItem(testItem);
		assertTrue("Item should not be in DB", db.getItem(addedItem) == null);
	}
	
	@Test
	public void testInsertDetailsWithoutConnect() {
		db.connect();
		db.close();		
		int addedItem = db.addItem("Leather Book","Book",material,20.0);
		assertTrue("Item should not be in DB", db.getItem(addedItem) == null);
	}
	
	@Test
	public void testInsertNull() {
		db.connect();
		Item item = new Item(null, null, null, 0);
		int addedItem = db.addItem(item);
		assertTrue("Item should exist", itemsEqual(db.getItem(addedItem), item));
		db.close();
	}
	
	@Test
	public void testInsertEmptyStrings() {
		db.connect();
		Item item = new Item("", "", material, 0);
		int addedItem = db.addItem(item);
		assertTrue("Item should exist", itemsEqual(db.getItem(addedItem), item));
		db.close();
	}
	
	@Test 
	public void testInsertMaxPrice() {
		db.connect();
		Item item = new Item("Leather Book", "Book", material, Double.MAX_VALUE);
		int addedItem = db.addItem(item);
		assertTrue("Item should exist", db.getItem(addedItem).getPrice() == Double.MAX_VALUE);
		db.close();
	}
	
	@Test 
	public void testInsertNegativePrice() {
		// Check for non-errors - maybe we should set to zero if negative value entered?
		db.connect();
		Item item = new Item("Leather Book", "Book", material, -1);
		int addedItem = db.addItem(item);
		assertTrue("Item should exist", itemsEqual(item, db.getItem(addedItem)));
		db.close();
	}
	
	@Test
	public void testInsertOneItem() {
		db.connect();
		int addedItem = db.addItem(testItem);
		assertTrue("Item should be in DB", itemsEqual(db.getItem(addedItem), testItem));
		db.close();
	}
	
	@Test
	public void testInsertOneItemByDetails() {
		db.connect();
		int addedItem = db.addItem("Leather Book", "Book", material, 10.0);
		Item item = db.getItem(addedItem);
		assertTrue("Item should be in DB",
				item != null &&
				materialsEqual(item.getMaterials(), material) &&
				item.getName().equals("Leather Book") &&
				item.getType().equals("Book") &&
				item.getPrice() == 10.0);
		db.close();
	}
	
	
	
	//********************************************
	//			Delete Tests
	//********************************************
	
	@Test
	public void testDeleteItemWithoutConnecting() {
		db.connect();
		int addedItem = db.addItem(testItem);
		db.close();
		assertTrue("Item should not be deleted", !db.deleteItem(addedItem));
	}
	
	@Test
	public void testDeleteItemById() {
		db.connect();
		int addedItem = db.addItem(testItem);
		db.deleteItem(addedItem);
		assertTrue("Item should not exist", db.getItem(addedItem) == null);
		db.close();
	}
	
	@Test 
	public void testDeleteItem() {
		db.connect();
		int addedItem = db.addItem(testItem);
		db.deleteItem(testItem);
		assertTrue("Item should not exist", db.getItem(addedItem) == null);
		db.close();
	}
	
	@Test
	public void testDeleteNullItem() {
		db.connect();
		int addedItem = db.addItem(null);
		db.deleteItem(addedItem);
		assertTrue("Item should not exists", db.getItem(addedItem) == null);
		db.close();
	}
	
	
}
