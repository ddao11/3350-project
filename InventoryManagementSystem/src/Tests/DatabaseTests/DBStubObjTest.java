package Tests.DatabaseTests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Database.DBObj;
import DomainObj.Item;
import DomainObj.Material;

public class DBStubObjTest {

	DBObj stub;
	ArrayList<Material> materials;
	@Before
	public void setUp() throws Exception {
		materials = new ArrayList<Material>();
		materials.add(new Material("Leather", 5.00));
		stub = new DBObj(0, new Item("Leather Book", "Book", materials, 10.0));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSetGetId() {
		stub.setId(5);
		assertTrue("Id should be 5.", stub.getId() == 5);
	}

	@Test
	public void testSetGetData() {
		Item i = new Item("Leather Journal", "Journal", null, 10.0);
		stub.setData(i);
		assertTrue("Data should match inserted data.", stub.getData() == i);
	}

	@Test
	public void testEqualsItem() {
		Item i = new Item("Leather Book", "Book", materials, 10.0);
		assertTrue("Stub should match item.", stub.equals(i));
	}

	@Test
	public void testCompareMaterials1() {
		Item i = new Item("Leather Book", "Book", materials, 10);
		Item i2 = new Item("Leather Book", "Book", materials, 10);
		assertTrue("Materials should be equal.", stub.compareMaterials(i, i2));
	}
	
	@Test
	public void testCompareMaterials2() {
		ArrayList<Material> materials2 = new ArrayList<Material>();
		materials2.add(new Material("Paper", 5.00));
		Item i = new Item("Leather Book", "Book", materials, 10);
		Item i2 = new Item("Leather Book", "Book", materials2, 10);
		assertFalse("Materials should not be equal.", stub.compareMaterials(i, i2));
	}

}
