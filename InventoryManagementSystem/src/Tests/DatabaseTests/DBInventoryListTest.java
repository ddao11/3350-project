package Tests.DatabaseTests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Database.DBControllerInterface;
//import Database.DBControllerStub;
import Database.DBHolder;
import DomainObj.Item;
import DomainObj.Material;

public class DBInventoryListTest {

	DBControllerInterface db;
	ArrayList<Material> material;
	Item testItem;
	int uniqueId = 0;
	
	@Before
	public void setUp() throws Exception {
		db = DBHolder.getDb();
		material = new ArrayList<Material>();
		testItem = createItem();
	}

	@After
	public void tearDown() throws Exception {
	}

	//********************************************
	//			Helper Methods
	//********************************************
	private Item createItem() {
		ArrayList<Material> tempMaterial = new ArrayList<Material>();
		tempMaterial.add(new Material("Leather", 10.00));
		return new Item("Leather Book", "Book", tempMaterial, 65.00);
	}
	/*
	private boolean detailsMatch(Item a, Item b) {
		return a.getMaterials() == b.getMaterials() &&
				a.getName() == b.getName() &&
				a.getType() == b.getType() &&
				a.getPrice() == b.getPrice();
	}*/
	
	private int getUniqueId() {
		uniqueId++;
		return uniqueId;
	}
	
	//********************************************
	//			Connection Tests
	//********************************************
	
	@Test
	public void testConnect() {
		db.connect();
		assertTrue("DB should be connected", db.isConnected());
		db.close();
	}
	
	@Test
	public void testClose() {
		db.close();
		assertTrue("DB should be disconnected", !db.isConnected());
	}
			
	//********************************************
	//			Insertion Tests
	//********************************************
	
	@Test
	public void testInsertItemWithoutConnecting() {
		db.close();
		int addedItem = db.addInventoryItem(getUniqueId());
		assertTrue("Item should not be in DB", db.getItem(addedItem) == null);
	}
	
	@Test
	public void testInsertIdWithStatusWithoutConnect() {
		db.close();		
		int addedItem = db.addInventoryItem(getUniqueId(), "Sold");
		assertTrue("Item should not be in DB", db.getItem(addedItem) == null);
	}
		
	@Test
	public void testInsertOneItem() {
		db.connect();
		int addedItem = db.addInventoryItem(getUniqueId());
		assertTrue("Item should be in DB", db.getInventoryItemById(addedItem) != null);
		db.close();
	}
	
	
	//********************************************
	//			Delete Tests
	//********************************************
	
	@Test
	public void testDeleteItemWithoutConnecting() {
		db.connect();
		int addedItem = db.addInventoryItem(getUniqueId());
		db.close();
		assertTrue("Item should not be deleted", !db.deleteItem(addedItem));
	}
	
	@Test
	public void testDeleteItemById() {
		db.connect();
		int addedItem = db.addInventoryItem(getUniqueId());
		db.deleteItem(addedItem);
		assertTrue("Item should not exist", db.getItem(addedItem) == null);
		db.close();
	}
	
	}
