package Tests.BusinessLogicTests;

import static org.junit.Assert.*;

import java.sql.Date;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import BusinessLogic.SalesCalculator;
import Database.DBControllerInterface;
import Database.DBHolder;
import Database.DBObj;
import DomainObj.DateRange;
import DomainObj.Inventory;
import DomainObj.Item;
import DomainObj.Status;

public class SalesCalculatorTest {
	DBControllerInterface db;
	SalesCalculator sc = new SalesCalculator();
	int testItemID;
	
	@Before
	public void setUp() {
		db = DBHolder.getDb();
		db.connect();
		db.emptyItems();
		testItemID = db.addItem(new Item("TestItem", "TestType", null, 10.00));
		db.close();
	}
	
	@Test
	public void testGetEtsyFees() {
		db.connect();
		db.emptyInventory();		
		for (int i = 0; i < 100; i++) {
			int id = db.addInventoryItem(testItemID, Status.SoldOnEtsy.toString());
			Inventory inv = (Inventory)db.getInventoryItemById(id).getData();
			inv.setStatusDate("2014-07-10");
			inv.setStatusPrice(10.00);
			db.updateInventory(inv);
		}
		
		// 100 items at 10.00 = $1000 - Etsy fees are 2%
		assertTrue("Should be 20.00", sc.getEtsyFees(new DateRange("2014-07-10", "2014-07-10")) == 20.0);
		db.close();
	}

	/*@Test
	public void testGetTotalSales() {
		for (int i = 0; i < 50; i++) {
			db.addInventoryItem(new InventoryItem(i,i,Status.SoldOnEtsy, Date.valueOf("2014-01-01"), 10.00));
		}
		for (int i = 0; i < 50; i++) {
			db.addInventoryItem(new InventoryItem(i,i,Status.SoldOther, Date.valueOf("2014-01-01"), 10.00));
		}
		
		
		// 100 items at 10.00 = $1000 
		assertTrue("Should be 1000.00", sc.getTotalSales(null) == 1000.0);
	}
	*/
	
	@Test
	public void testGetTotalSales() {
		db.connect();
		db.emptyInventory();		
	
		for (int i = 0; i < 50; i++) {
			int id = db.addInventoryItem(testItemID, Status.SoldOnEtsy.toString());
			Inventory inv = (Inventory)db.getInventoryItemById(id).getData();
			inv.setStatusDate("2014-07-10");
			inv.setStatusPrice(10.00);
			db.updateInventory(inv);
		}
		
		for (int i = 0; i < 50; i++) {
			int id = db.addInventoryItem(testItemID, Status.SoldOther.toString());
			Inventory inv = (Inventory)db.getInventoryItemById(id).getData();
			inv.setStatusDate("2014-07-10");
			inv.setStatusPrice(10.00);
			db.updateInventory(inv);
		}
		
		for (int i = 0; i < 10; i++) { // add 10 items that are too early 
			int id = db.addInventoryItem(testItemID, Status.SoldOther.toString());
			Inventory inv = (Inventory)db.getInventoryItemById(id).getData();
			inv.setStatusDate("2014-07-09");
			inv.setStatusPrice(10.00);
			db.updateInventory(inv);
		}
		
		for (int i = 0; i < 10; i++) { // add 10 items that are too late
			int id = db.addInventoryItem(testItemID, Status.SoldOther.toString());
			Inventory inv = (Inventory)db.getInventoryItemById(id).getData();
			inv.setStatusDate("2014-07-11");
			inv.setStatusPrice(10.00);
			db.updateInventory(inv);
		}
		
		
		// 100 items at 10.00 = $1000
		assertTrue("Should be 1000.00", sc.getTotalSales(new DateRange("2014-07-10", "2014-07-10")) == 1000.0);
		db.close();
	}
	
	@After
	public void cleanUp(){
		
		db.emptyInventory();
	}
	
	/*************************************
	 * Helper Methods
	 *************************************/
	
	
}
