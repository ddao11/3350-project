package Tests.BusinessLogicTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import junit.framework.TestSuite;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import BusinessLogic.Search;
import Database.DBControllerInterface;
import Database.DBHolder;
import DomainObj.Inventory;
import DomainObj.Item;
import DomainObj.Material;

public class SearchTest extends TestSuite {
	Search search;
	DBControllerInterface db;
	ArrayList<Material> material;
	Item testItem;
	
	@Before
	public void setUp() throws Exception {
		db = DBHolder.getDb(); //new DBInventoryList();
		material = new ArrayList<Material>();
		search = new Search();	
	}

	@After
	public void tearDown() throws Exception {
	}

	//********************************************
	//			Helper Methods
	//********************************************

	
	//********************************************
	//			Connection Tests
	//********************************************
	
	
	@Test
	public void searchByStatus() {
		//db.close();
		db.connect();
		ArrayList<Inventory> testArrayList;
		Inventory testItem;
		testArrayList = search.getInventoryItemByStatus("Available");
		testItem = testArrayList.get(0);
		
		assertEquals("Available", testItem.getStatus());
		assertEquals(1, testItem.getInventoryID());
	}
	
	@Test
	public void testSearchById() {
		//db.close();
		db.connect();
		ArrayList<Inventory> testArrayList;
		Inventory testItem;
		testArrayList = search.getInventoryItemByID(2);
		testItem = testArrayList.get(0);
		
		assertTrue("Status should be lost.", testItem.getStatus().equals("Lost"));
		//assertEquals("Lost", testItem.getStatus());
		//assertEquals(2, testItem.getID());
		assertEquals(2, testItem.getInventoryID());
	}
	
	@Test
	public void testBooleansearchById(){
		db.close();
		boolean flag=false;
		flag = search.searchById(2);
		
		assertTrue("Search class should find id=2 inventory", flag);
	}
	
	@Test
	public void testBooleansearchByStatus(){
		db.close();
		boolean flag=false;
		//boolean flag2=false;
		flag = search.searchByStatus("Lost");
		//flag2= instance.searchByStatus("other");
		assertTrue("Search class should find status=2 inventory", flag);
		//assertFalse("Search class should find status=2 inventory", flag2);
	}
	
}
