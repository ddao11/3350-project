package Tests.IntegrationTests;
  
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.junit.Before;
import org.junit.Test;

import BusinessLogic.AccessDB;
import Database.DBControllerFinal;
import Database.DBControllerInterface;
import Database.DBControllerStub;
import Database.DBHolder;
import DomainObj.DeleteItem;
import DomainObj.Inventory;
import DomainObj.Item;
import DomainObj.Material;
import DomainObj.Status;

public class AccessDBStubTests {

	Table itemTable;
	Table inventoryTable;
	
	DBControllerInterface db = new DBControllerStub();
	
	@Before
	public void setUp() throws Exception {
		AccessDB.setDb(db);
	}

	@Test
	public void testLoadInventoryList() {
		Table invTable = createInventoryTable();
		Table itemTable = createItemTable();
		
		emptyDb();
		
		AccessDB.addItem(createItem());
		AccessDB.loadList(itemTable);
		itemTable.getItem(0).setChecked(true);
		AccessDB.addInventory(itemTable, invTable);
		
		assertTrue(invTable.getItem(0).getText(1).equals("Leather Book"));
	}

	@Test
	public void testLoadList() {
		Table itemTable = createItemTable();
		
		emptyDb();
		
		AccessDB.addItem(createItem());
		AccessDB.loadList(itemTable);
		assertTrue(itemTable.getItem(0).getText(1).equals("Leather Book"));
	}

	@Test
	public void testLoadByList() {
		Table table = createInventoryTable();
		emptyDb();
		db.connect();
		int itemId = db.addItem(createItem());
		ArrayList<Inventory> inventory = new ArrayList<Inventory>();
		inventory.add(new Inventory(itemId, Status.Available, 100, "2014-07-11", 40.00));
		AccessDB.loadByList(inventory, table);
		assertTrue(table.getItem(0).getText(1).equals("Leather Book"));
		
	}

	@Test
	public void testDeleteInventoryItems() {
		Table invTable = createInventoryTable();
		Table itemTable = createItemTable();
		
		emptyDb();
		
		AccessDB.addItem(createItem());
		AccessDB.loadList(itemTable);
		itemTable.getItem(0).setChecked(true);
		AccessDB.addInventory(itemTable, invTable);
		invTable.getItem(0).setChecked(true);
		AccessDB.deleteInventoryItems(invTable);
		
		assertTrue(invTable.getItems().length == 0);
	}

	
	@Test
	public void testDeleteItems() {
		emptyDb();
		Table table = createItemTable();
		ArrayList<DeleteItem> deleteItems = new ArrayList<DeleteItem>();
		int id = AccessDB.addItem(createItem());
		AccessDB.loadList(table);
		
		table.getItem(0).setChecked(true);
		deleteItems.add(new DeleteItem(0, Integer.parseInt(table.getItem(0).getText(0)), table.getItem(0).getText(1)));
		AccessDB.deleteItems(deleteItems);
		
		assertTrue(db.getItem(id) == null);
		
	}

	@Test
	public void testGetAllMaterials() {
		db.connect();
		ArrayList<Material> dbMaterials = db.getAllMaterials();
		db.close();
		ArrayList<Material> accessDbMaterials = AccessDB.getAllMaterials();
		
		assertTrue(materialsEqual(dbMaterials, accessDbMaterials));
		
		
	}

	@Test
	public void testUpdateItem() {
		emptyDb();
		int id = AccessDB.addItem(createItem());
		
		db.connect();
		Item item = db.getItem(id);
		db.close();
		
		item.setName("Awesome Red Journal");
		AccessDB.updateItem(id, item);
		db.connect();
		assertTrue(db.getItem(id).getName().equals("Awesome Red Journal"));
		db.close();
	}
	
	/*********************************
	 * 			Helper Methods
	 ********************************/
	
	private Table createItemTable() {
		Shell shell = new Shell();
		
		itemTable = new Table(shell,SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		itemTable.setBounds(40, 40, 400, 500);
		itemTable.setLinesVisible(true);
		itemTable.setHeaderVisible(true);
		
		final TableColumn id = new TableColumn(itemTable, SWT.NONE);
		id.setWidth(25);
		id.setText("");
		
		final TableColumn itemName = new TableColumn(itemTable, SWT.NONE);
		itemName.setWidth(125);
		itemName.setText("Item Name");
		
		final TableColumn type = new TableColumn(itemTable, SWT.NONE);
		type.setWidth(125);
		type.setText("Type");
		
		final TableColumn price = new TableColumn(itemTable, SWT.NONE);
		price.setWidth(120);
		price.setText("price");
		
		return itemTable;
	}
	
	private Table createInventoryTable() {
		Shell shell = new Shell();
		
		inventoryTable = new Table( shell,SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		inventoryTable.setBounds(40, 40, 400, 330);
		inventoryTable.setLinesVisible(true);
		inventoryTable.setHeaderVisible(true);
		
		
		final TableColumn id = new TableColumn(inventoryTable, SWT.NONE);
		id.setWidth(80);
		id.setText("Inv�entory ID");
		
		final TableColumn itemName = new TableColumn(inventoryTable, SWT.NONE);
		itemName.setWidth(140);
		itemName.setText("Item Name");
		
		final TableColumn type = new TableColumn(inventoryTable, SWT.NONE);
		type.setWidth(140);
		type.setText("status");
		
		return inventoryTable;
	}

	private Item createItem() {
		return new Item("Leather Book", "Book", null, 65.00);
	}
	
	private boolean materialsEqual(ArrayList<Material> m1, ArrayList<Material> m2) {
		boolean equal = true;
		int m1Size = (m1 == null) ? 0 : m1.size();
		int m2Size = (m2 == null) ? 0 : m2.size();
		
		if (m1Size != m2Size) {
			equal = false;
		}
		else if (m1Size == 0 && m2Size == 0) {
			equal = true;
		}
		else {
			for (int i = 0; i < m1.size() && equal; i++) {
				if (!m1.get(i).equals(m2.get(i))) {
					equal = false;
				}
			}
		}
		
		return equal;
	}
	
	private String isNull(String value, String nullValue) {
		return (value != null) ? value : nullValue;
	}
	
	private boolean itemsEqual(Item i1, Item i2) {
		//boolean equal = true;
		String i1Name = isNull(i1.getName(), "null");
		String i2Name = isNull(i2.getName(), "null");
		String i1Type = isNull(i1.getType(), "null");
		String i2Type = isNull(i2.getType(), "null");
		
		return (
				i1Name.equals(i2Name) &&
				i1Type.equals(i2Type) &&
				materialsEqual(i1.getMaterials(), i2.getMaterials()) &&
				i1.getPrice() == i2.getPrice()
		);
						
				
						
	}
	
	private void emptyDb() {
		db.connect();
		db.emptyItems();
		db.emptyInventory();
		db.close();
	}
	
}
