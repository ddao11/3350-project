package Database;

import DomainObj.Item;

public class DBObj {
	int id;
	Object data;
	
	public DBObj(int id, Object data){
		setId(id);
		setData(data);
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public void setData(Object data){
		this.data = data;
	}
	
	public int getId(){
		return id;
	}
	
	public Object getData(){
		return data;
	}
	
	public boolean equals(Item data){
		boolean result = false;
		
		if( this.data instanceof Item && data instanceof Item ){
			Item comp1 = (Item)this.data;
			Item comp2 = data;
			
			result = (comp1.getName().equals(comp2.getName()) &&
					comp1.getType().equals(comp2.getType()) &&
					comp1.getPrice() == comp2.getPrice() && 
					compareMaterials(comp1, comp2)); 
		}
		
		return result;
	}
	
	public boolean compareMaterials(Item comp1, Item comp2){
		boolean result = false;
		
		if(comp1.getMaterials().size() == comp2.getMaterials().size()){
			result = true;
			for(int i = 0; i < comp1.getMaterials().size(); i++){
				result = result && comp1.getMaterials().get(i).equals(comp2.getMaterials().get(i));
			}
		}
				
		return result;
	}
	
	public String toString(){
		String itemStr = data.toString();
		return id + " " + itemStr; 
	}
}
