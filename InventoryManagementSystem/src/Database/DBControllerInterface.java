package Database;

import java.util.ArrayList;

import DomainObj.DateRange;
import DomainObj.Inventory;
import DomainObj.Item;
import DomainObj.Material;
import DomainObj.Status;

public interface DBControllerInterface {
	public void close();
	public void connect();
	public boolean isConnected();
	public ArrayList<Item> getItems();
	public ArrayList<DBObj> getDBItems();
	public ArrayList<Inventory> getInventoryItems(Status statusCode, DateRange range);
	public ArrayList<Material> getAllMaterials();
	public int addItem(String name, String type, ArrayList<Material> material, double price);
	public int addItem(Item item);
	public boolean deleteItem(int id);
	public boolean deleteItem(Item item);
	public DBObj getDbObj(int id);
	public Item getItem(int id);
	public boolean updateItem(DBObj item);
	public boolean updateInventory(Inventory invItem);
	public ArrayList<Inventory> getInventory();
	public DBObj getInventoryItemById(int id);
	public boolean deleteInventoryItem(int id);
	public ArrayList<DBObj> getDBInventoryItems();
	public int addInventoryItem(int itemID, String status);
	public int addInventoryItem(int itemID);
	public boolean emptyInventory();
	public boolean emptyItems();
	public boolean changeStatus(int id, String status);
}