package Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import DomainObj.DateRange;
import DomainObj.Inventory;
import DomainObj.Item;
import DomainObj.Material;
import DomainObj.Status;

public class DBControllerFinal implements DBControllerInterface{
	private Connection conn;
	private String dbType;
	private String cmdString;
	
	public DBControllerFinal(){}
	
	@Override
	public void close() {
		if(isConnected())
		{
			try
			{	// commit all changes to the database
				ResultSet rs;
				cmdString = "shutdown compact";
				rs = conn.createStatement().executeQuery(cmdString);
				rs.close();
				conn.close();
			}
			catch (Exception e)
			{
				processSQLError(e);
			}
		}
		System.out.println("Closed " + dbType +" database");
	}

	@Override
	public void connect() {
		String url;
		if(conn == null || !isConnected())
		{
			try
			{
				// Setup for HSQL
				dbType = "HSQL";
				Class.forName("org.hsqldb.jdbcDriver").newInstance();
				url = "jdbc:hsqldb:database/IMS"; // stored on disk mode
				conn = DriverManager.getConnection(url, "SA", "");
				
				System.out.println("Opened " + dbType +" database");
			}
			catch (Exception e)
			{
				processSQLError(e);
			}
		}
	}

	@Override
	public boolean isConnected() {
		boolean result = false;
		
		try {
			result = !conn.isClosed();
		} catch (SQLException e) {
			processSQLError(e);
		}
		
		return result;
	}

	@Override
	public ArrayList<Item> getItems() {
		ResultSet rs;
		ArrayList<Item> result = new ArrayList<Item>();
		int itemId;
		String type;
		String name;
		double cost;
		
		try
		{
			cmdString = "SELECT * " +
						"FROM Item";
			rs = conn.createStatement().executeQuery(cmdString);
			while (rs.next())
			{
				itemId = rs.getInt("ID");
				name = rs.getString("Name");
				type = rs.getString("Type");
				cost = rs.getDouble("Cost");
				
				result.add(new Item(name, type, getMaterial(itemId) ,cost));
			}
			rs.close();
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
		
		return result;
	}

	@Override
	public ArrayList<DBObj> getDBItems() {
		ResultSet rs;
		ArrayList<DBObj> result = new ArrayList<DBObj>();
		int itemId;
		String type;
		String name;
		double cost;
		
		try
		{
			cmdString = "SELECT * " +
						"FROM Item";
			rs = conn.createStatement().executeQuery(cmdString);
			while (rs.next())
			{
				itemId = rs.getInt("ID");
				name = rs.getString("Name");
				type = rs.getString("Type");
				cost = rs.getDouble("Cost");
				
				result.add(new DBObj(itemId, new Item(name, type, getMaterial(itemId) ,cost)));
			}
			rs.close();
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
		
		return result;
	}

	@Override
	public int addItem(String name, String type, ArrayList<Material> material,double price) {
		return addItem(new Item(name, type, material, price));
	}

	@Override
	public int addItem(Item item) {
		int id = -1;
		ResultSet rs;
		
		try
		{
			cmdString = "INSERT INTO item(type, name, cost) " +
						"VALUES('" + item.getType() + "', '" + item.getName() + "', " + item.getPrice() + ")";
			rs = conn.createStatement().executeQuery(cmdString);			
			rs.close();
			
			id = getItemId(item);
			if(!addMaterials(item.getMaterials(), id)){
				deleteItem(id);
				id = -1;
			}
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
		
		return id;
	}

	@Override
	public boolean deleteItem(int id) {
		boolean result = false;
		ResultSet rs;
		
		try
		{
			cmdString = "DELETE " +
						"FROM Item " + 
						"WHERE id = " + id;
			rs = conn.createStatement().executeQuery(cmdString);
			result = true;
			rs.close();
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
		
		return result;
	}

	@Override
	public boolean deleteItem(Item item) {
		boolean result = false;
		ResultSet rs;
		
		try
		{
			cmdString = "DELETE " +
						"FROM Item " + 
						"WHERE name = '" + item.getName() + "' " +
						"AND cost = " + item.getPrice() + " " + 
						"AND type = '" + item.getType() + "'";
			rs = conn.createStatement().executeQuery(cmdString);
			result = true;
			rs.close();
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
		
		return result;
	}

	@Override
	public Item getItem(int id) {
		Item result = null;
		
		ResultSet rs;
		int itemId;
		String type;
		String name;
		double cost;
		
		try
		{
			cmdString = "SELECT * " +
						"FROM Item " + 
						"WHERE id = " + id;
			rs = conn.createStatement().executeQuery(cmdString);
		
			while (rs.next()) {
				itemId = rs.getInt("ID");
				name = rs.getString("Name");
				type = rs.getString("Type");
				cost = rs.getDouble("Cost");
				result = new Item(name, type, getMaterial(itemId) ,cost);
			}
			
			
			rs.close();
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
		
		return result;
	}
	
	@Override
	public ArrayList<Material> getAllMaterials() {
		ResultSet rs;
		ArrayList<Material> result = new ArrayList<Material>();
		String name;
		Double cost;
		
		cmdString = "SELECT * " +
				"FROM Material";
		try {
			rs = conn.createStatement().executeQuery(cmdString);
		
			while (rs.next())
			{
				name = rs.getString("Name");
				cost = rs.getDouble("Cost");
				
				result.add(new Material(name, cost));
			}
			rs.close();
		} catch (SQLException e) {
			processSQLError(e);
		}	
		return result;
	}

	@Override
	public DBObj getDbObj(int id) {
		DBObj result = null;
		ResultSet rs;
		int itemId;
		String type;
		String name;
		double cost;
		
		try
		{
			cmdString = "SELECT * " +
						"FROM Item " + 
						"WHERE id = " + id;
			rs = conn.createStatement().executeQuery(cmdString);
		
			while (rs.next()){
				itemId = rs.getInt("ID");
				name = rs.getString("Name");
				type = rs.getString("Type");
				cost = rs.getDouble("Cost");
				result = new DBObj(itemId, new Item(name, type, getMaterial(itemId) ,cost));
			}
			
			rs.close();
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
		
		return result;
	}
	
	public boolean updateItem(DBObj dbObj){
		boolean result = false;
		Item item = (Item)dbObj.getData();
		ResultSet rs;
		
		try
		{
			cmdString = "UPDATE Item " +
						"SET name='" + item.getName() + "'," +
						"type='" + item.getType() + "'," +
						"cost='" + item.getPrice() + "' " +
						"WHERE id = " + dbObj.getId();
			rs = conn.createStatement().executeQuery(cmdString);
			result = true;
			rs.close();
			
			updateMaterials(item.getMaterials(), dbObj.getId());
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
		
		return result;
	}

	@Override
	public ArrayList<Inventory> getInventory() {
		ArrayList<Inventory> result = new ArrayList<Inventory>();
		
		ResultSet rs;
		int itemId;
		String statusCode;
		int id;
		
		try
		{
			cmdString = "SELECT ID, ItemId, statusCode " +
						"FROM Inventory";
			rs = conn.createStatement().executeQuery(cmdString);
		
			while (rs.next()){
				itemId = rs.getInt("ItemId");
				statusCode = rs.getString("statusCode");
				id = rs.getInt("id");
				result.add(new Inventory(itemId, statusCode, id));
			}
			
			rs.close();
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
		
		return result;
	}

	@Override
	public DBObj getInventoryItemById(int id) {
		DBObj result = null;
		ResultSet rs;
		int itemId;
		String statusCode;
		
		try
		{
			cmdString = "SELECT * " +
						"FROM Inventory " + 
						"WHERE id = " + id;
			rs = conn.createStatement().executeQuery(cmdString);
		
			while (rs.next()){
				itemId = rs.getInt("ItemID");
				statusCode = rs.getString("statusCode");
				result = new DBObj(id, new Inventory(itemId, statusCode, id));
			}
			
			rs.close();
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
	
		return result;

	}

	@Override
	public boolean deleteInventoryItem(int id) {
		boolean result = false;
		ResultSet rs;
		
		try
		{
			cmdString = "DELETE " +
						"FROM Inventory " + 
						"WHERE id = " + id;
			rs = conn.createStatement().executeQuery(cmdString);
			result = true;
			rs.close();
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
		
		return result;
	}

	@Override
	public ArrayList<DBObj> getDBInventoryItems() {
		ArrayList<DBObj> result = new ArrayList<DBObj>();

		ResultSet rs;
		int itemId;
		String statusCode;
		int id;
		
		try
		{
			cmdString = "SELECT * " +
						"FROM Inventory ";
			rs = conn.createStatement().executeQuery(cmdString);
		
			while (rs.next()){
				id = rs.getInt("ID");
				itemId = rs.getInt("itemId");
				statusCode = rs.getString("statusCode");
				result.add(new DBObj(id, new Inventory(itemId, statusCode, id)));
			}
			
			rs.close();
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
		
		return result;
	}

	@Override
	public int addInventoryItem(int itemID, String status) {
		int id = -1;
		ResultSet rs;
		
		try
		{
			cmdString = "INSERT INTO Inventory(ItemId, statusCode, statusDate) " +
						"VALUES(" + itemID + ", '" + status + "', CURRENT_DATE)";
			rs = conn.createStatement().executeQuery(cmdString);			
			rs.close();
			
			id = getInventoryItemId(itemID, status);
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
		
		return id;
	}

	@Override
	public int addInventoryItem(int itemID) {
		return addInventoryItem(itemID, "Available");
	}

	/*@Override
	public void addInventoryItem(InventoryItem item) {
		// TODO Auto-generated method stub
	}

	@Override
	public ArrayList<InventoryItem> getInventoryItems() {
		// TODO Auto-generated method stub
		return new ArrayList<InventoryItem>();
	}*/
	
	@Override
	public ArrayList<Inventory> getInventoryItems(Status statusCode, DateRange range) {
		
		ArrayList<Inventory> result = new ArrayList<Inventory>();
		
		ResultSet rs;
				
		try
		{
			cmdString = "SELECT * " +
						"FROM Inventory " +
						"JOIN Item " +
						"ON Inventory.itemID = Item.ID " +
						"WHERE statusCode = '" + statusCode.toString() + "' ";
			rs = conn.createStatement().executeQuery(cmdString);
		
			while (rs.next()){
				int itemID = rs.getInt("itemID");
				String status = rs.getString("statusCode");
				int id = rs.getInt("id");
				String date = rs.getString("statusDate");
				double price = rs.getDouble("Cost");
				
				if (date.compareTo(range.getFrom()) >= 0 &&
						date.compareTo(range.getTo()) <= 0) {
					result.add(new Inventory(itemID, Status.valueOf(status),id, date, price ));	
				}
				
			}
			
			rs.close();
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
		
		
		
		return result;
	}
	
	public boolean updateInventory(Inventory invItem) {
		boolean result = false;
		ResultSet rs;
		
		try
		{
			cmdString = "UPDATE Inventory " +
						"SET itemID='" + invItem.getID() + "'," +
						"statuscode='" + invItem.getStatus() + "'," +
						"statusdate='" + invItem.getStatusDate() + "' " +
						"WHERE id = " + invItem.getInventoryID();
			rs = conn.createStatement().executeQuery(cmdString);
			result = true;
			rs.close();
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
		
		return result;
	}
	
	//************************************************************
	//					INTERNAL METHODS
	//************************************************************
	
	private void processSQLError(Exception e)
	{
		System.out.println("***** SQL Error: *****\n" + e.getMessage() + "\n");
		//e.printStackTrace();
	}
	
	private ArrayList<Material> getMaterial(int itemId) throws SQLException{
		ResultSet rs;
		ArrayList<Material> result = new ArrayList<Material>();
		String name;
		double cost;
		
		cmdString = "SELECT * " +
					"FROM Material m JOIN ItemMaterial im " +
						"ON m.ID = im.MaterialID " +
					"WHERE im.ItemId = " + itemId;
		rs = conn.createStatement().executeQuery(cmdString);
		
		while (rs.next())
		{
			name = rs.getString("Name");
			cost = rs.getDouble("Cost");
			
			result.add(new Material(name, cost));
		}
		rs.close();
		
		return result;
	}
	
	private int getItemId(Item item) throws SQLException{
		int id = -1;
		ResultSet rs;
		
		cmdString = "Select ID " + 
					"FROM Item " +
					"WHERE name = '" + item.getName() + "' " +
					"AND type = '" + item.getType() + "' " +
					"AND cost = " + item.getPrice();
		rs = conn.createStatement().executeQuery(cmdString);
		
		while (rs.next()){
			id = rs.getInt("ID");
		}
		
		rs.close();
		
		return id;
	}
	
	private int getInventoryItemId(int itemId, String status) throws SQLException{
		int id = -1;
		ResultSet rs;
		
		cmdString = "Select ID " + 
					"FROM Inventory " +
					"WHERE itemId = " + itemId + " " +
					"AND statusCode = '" + status + "' ";
					
		rs = conn.createStatement().executeQuery(cmdString);
		
		while (rs.next()){
			id = rs.getInt("ID");
		}
		
		rs.close();
		
		return id;
	}
	
	private boolean addMaterials(ArrayList<Material> materials, int itemId) throws SQLException{
		ResultSet rs;
		boolean result = true;
		
		for(Material material:materials){
			int materialId = getMaterialId(material);
			
			if(materialId != -1){
				cmdString = "INSERT INTO ItemMaterial(ItemId, MaterialId) " +
							"VALUES(" + itemId + ", " + materialId + ")";
				rs = conn.createStatement().executeQuery(cmdString);
				rs.close();
			}
			result = result && (materialId != -1);
		}
		return result;
	}
	
	private int getMaterialId(Material material) throws SQLException{
		int id = -1;
		ResultSet rs;
		
		cmdString = "Select ID " + 
					"FROM Material " +
					"WHERE name = '" + material.getName() + "' " +
					"AND cost = " + material.getCost();
		rs = conn.createStatement().executeQuery(cmdString);
		
		while (rs.next()){
			id = rs.getInt("ID");
		}
		
		rs.close();
		
		return id;
	}
	
	private boolean updateMaterials(ArrayList<Material> materials, int itemId) throws SQLException{
		boolean result = false;
		
		if(deleteMaterials(itemId)){
			result = addMaterials(materials, itemId);
		}
		return result;
	}
	
	private boolean deleteMaterials(int itemId){
		boolean result = false;
		ResultSet rs;
		
		try
		{
			cmdString = "DELETE FROM ItemMaterial " +
						"WHERE ItemID = " + itemId;
			rs = conn.createStatement().executeQuery(cmdString);
			result = true;
			rs.close();
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
		
		return result;
	}

	/************************************
	 * 	Methods used in Unit tests
	 ************************************/
	
	@Override
	public boolean emptyInventory() {
		boolean result = false;
		ResultSet rs;
		
		try
		{
			cmdString = "DELETE FROM Inventory ";
			rs = conn.createStatement().executeQuery(cmdString);
			result = true;
			rs.close();
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
		
		return result;
		
	}

	@Override
	public boolean emptyItems() {
		boolean result = false;
		ResultSet rs;
		
		try
		{
			cmdString = "DELETE FROM Item ";
			rs = conn.createStatement().executeQuery(cmdString);
			result = true;
			rs.close();
		}
		catch (Exception e)
		{
			processSQLError(e);
		}
		
		return result;
		
		
	}

	@Override
	public boolean changeStatus(int id, String status) {
		// TODO Auto-generated method stub
		return false;
	}

}
