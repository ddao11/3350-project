package Database;

import java.util.ArrayList;

import org.eclipse.swt.widgets.Shell;

import DomainObj.Inventory;

public class Search {
	
	protected Object data;
	protected DBControllerInterface db;
	protected Shell shell;
	private ArrayList<Inventory> dbArrayList_init;
	private ArrayList<Inventory> resultListID;
	private ArrayList<Inventory> resultListStatus;
	private ArrayList<Inventory> resultListName;
	
	public Search(){
		data=null;
		db = DBHolder.getDb();
		db.connect();
		this.dbArrayList_init=db.getInventory();
	}
	
	public ArrayList<Inventory> getItemByInput(String id, String name, String status){
		ArrayList<Inventory> resultList=dbArrayList_init;
		if(!name.equals("")){
			resultList=getItemByName(resultList,name);
		}
		if(!status.equals("")){
			resultList=getItemByStatus(resultList,status);
		}
		if(!id.equals("")){
			resultList=getItemByID(resultList,id);
			System.out.println(resultList.size());
		}

			
		
		return resultList;
	}
	
	
	public ArrayList<Inventory> getItemByStatus(ArrayList<Inventory> dbArrayList, String status){
		resultListStatus=new ArrayList<Inventory>();
		if(dbArrayList!=null){
			for(int i = 0; i < dbArrayList.size(); i++){
				if(dbArrayList.get(i).getStatus().equals(status)){
					resultListStatus.add(dbArrayList.get(i));
				}
			}
		}
		return resultListStatus;
	}
	
	public ArrayList<Inventory> getItemByID(ArrayList<Inventory> dbArrayList,String id){
			resultListID=new ArrayList<Inventory>();
			if(dbArrayList!=null)
			for(int i = 0; i < dbArrayList.size(); i++){
				
				
				if((""+dbArrayList.get(i).getInventoryID()).contains(id)){
					resultListID.add(dbArrayList.get(i));
					
				}
			}
		return resultListID;
	}
	
	public ArrayList<Inventory> getItemByName(ArrayList<Inventory> dbArrayList,String name){
		resultListName=new ArrayList<Inventory>();
		if(dbArrayList!=null)
			for(int i = 0; i < dbArrayList.size(); i++){
				System.out.println(((Inventory) dbArrayList.get(i)).getName());
				if(((Inventory)dbArrayList.get(i)).getName().toLowerCase().contains(name.toLowerCase())){
					resultListName.add(dbArrayList.get(i));
				}
			}
		return resultListName;
	}
	
	
	
}
