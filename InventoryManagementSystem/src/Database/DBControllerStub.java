package Database;

import java.util.ArrayList;

import Database.DBControllerInterface;
import Database.DBObj;
import DomainObj.DateRange;
import DomainObj.Inventory;
import DomainObj.Item;
import DomainObj.Material;
import DomainObj.Status;

public class DBControllerStub implements DBControllerInterface{
	private boolean connected = false;
	private ArrayList<DBObj> dbArrayList;
	private ArrayList<DBObj> dbInventoryArrayList;
//	private ArrayList<InventoryItem> inventoryList;
	private static int idCount = 0; //autoincrement id as in real db
	
	public DBControllerStub(){
		initDb();
	}
	
	@Override
	public boolean updateItem(DBObj item) {
		return true;
	}
	
	@Override
	public void close() {
		connected = false;
		System.out.println("Closed Stub Database");
	}

	@Override
	public void connect() {
		connected = true;
		System.out.println("Opened Stub Database");
	}

	@Override
	public boolean isConnected() {
		return connected;
	}
	
	public ArrayList<DBObj> getDBItems() {
		return dbArrayList;
	}
	
	@Override
	public ArrayList<Item> getItems() {
		ArrayList<Item> result = new ArrayList<Item>();
		
		for(DBObj obj:dbArrayList){
			result.add((Item)obj.getData());
		}
		
		return result;
	}
	
	@Override
	public int addItem(String name, String type, ArrayList<Material> material, double price) {
		int id = -1;
		if(connected){
			id = getIdCount();
			dbArrayList.add(new DBObj(id, new Item(name, type, material, price)) );
		}
			
		return id; //no real issues to prevent from adding to ArrayList
	}
	
	@Override
	public int addItem(Item item) {
		int id = -1;
		if(connected){
			id = getIdCount();
			dbArrayList.add(new DBObj(id, item));
		}
			
		return id; //no real issues to prevent from adding to ArrayList
	}

	@Override
	public boolean deleteItem(int id) {
		boolean result = false;
		DBObj data = null;
		if(connected)
		{
			data = getItemById(id);
			if(data != null){
				result = dbArrayList.remove(data);
			}
		}
		return result;
	}

	@Override
	public boolean deleteItem(Item item) {
		boolean result = false;
		DBObj data = null;
		if(connected)
		{
			data = getItemByData(item);
			if(data != null){
				result = dbArrayList.remove(data);
			}
		}
		
		return result;
	}
	
	@Override
	public Item getItem(int id) {
		Item result = null;
		DBObj data;
		
		if(connected)
		{
			data = getItemById(id);
			if(data != null)
				result = (Item)data.getData();
		}
		
		return result;
	}

	// return all materials in the Material Table
	public ArrayList<Material> getAllMaterials() {
		ArrayList<Material> materials = new ArrayList<Material>();
		
		materials.add(new Material("Wood", 10.00));
		materials.add(new Material("Thread", 3.00));
		materials.add(new Material("Leather", 25.00));
		materials.add(new Material("SketchPaper", 10.00));
		materials.add(new Material("floppy Disk", 0.50));
		materials.add(new Material("Duct Tape", 0.50));
		materials.add(new Material("Buckle", 2.00));
		
		return materials;
	}
	
	@Override
	public DBObj getDbObj(int id) {
		return getItemById(id);
	}
	
	@Override
	public ArrayList<Inventory> getInventoryItems(Status statusCode, DateRange range) {
		ArrayList<Inventory> invItems = new ArrayList<Inventory>();
		
		// use whatever is in the inventory list currently
		// but if it's empty, initialize it.
//		if (inventoryList == null)
//			inventoryList = new ArrayList<InventoryItem>();
		
		/*if (dbInventoryArrayList.size() == 0) {
			// create 10 random items of inventory
			for (int i = 0; i < 10; i++) {
				int id = getIdCount();
				dbInventoryArrayList.add(new DBObj(id, new Inventory(i, statusCode.toString())));//, range.getTo(), 50.00 + i)));
			}
		}
		*/
				
//		for (Inventory i : dbInventoryArrayList) {
//			if (i.getStatus() == statusCode) {
//				inventory.add(i);
//			}
//		}
		
		for (DBObj obj : dbInventoryArrayList) {
			Inventory i = (Inventory)obj.data;
			if (i.getStatus() != null && i.getStatusDate() != null && i.getStatusPrice() > 0) {
				if (statusCode.toString().equals(i.getStatus())) {
					if (i.getStatusDate().compareTo(range.getFrom()) >= 0) {
						if (i.getStatusDate().compareTo(range.getTo()) <= 0) {
							// item is correct status and within date range
							invItems.add(i);
						}
					}
				}
			}
		}
		
		return invItems;
	}
	
	public ArrayList<Inventory> getInventory() {
		ArrayList<Inventory> result = new ArrayList<Inventory>();
		
		for(DBObj obj:dbInventoryArrayList){
			result.add((Inventory)obj.getData());
		}
		
		return result;
	}
	
	public boolean deleteInventoryItem(int id) {
		boolean result = false;
		DBObj data = null;
		if(connected)
		{
			data = getInventoryItemById(id);
			if(data != null){
				result = dbInventoryArrayList.remove(data);
			}
		}
		return result;
	}
	
	public boolean deleteItem(Inventory item) {
		boolean result = false;
		DBObj data = null;
		if(connected)
		{
			data = getItemByData(item);
			if(data != null){
				result = dbArrayList.remove(data);
			}
		}
		
		return result;
	}
	
	public ArrayList<DBObj> getDBInventoryItems() {
		return dbInventoryArrayList;
	}	
	
	public int addInventoryItem(int itemID, String status) {
		int id = -1;
		if(connected){
			id = getIdCount();
			Inventory newInventory=new Inventory(itemID, status);
			newInventory.setInventoryID(id);
			dbInventoryArrayList.add(new DBObj(id, newInventory));
		}
			
		return id; //no real issues to prevent from adding to ArrayList
	}
	
	@Override
	public int addInventoryItem(int itemID) {
		int id = -1;
		if(connected){
			id = getIdCount();
			Inventory newInventory=new Inventory(itemID, "");
			newInventory.setInventoryID(id);
			dbArrayList.add(new DBObj(id, newInventory));
		}
			
		return id; //no real issues to prevent from adding to ArrayList
	}

	@Override
	public boolean updateInventory(Inventory invItem) {
		boolean result = false;

		for (DBObj o : dbInventoryArrayList) {
			Inventory i = (Inventory)o.getData();
			if (i.getInventoryID() == invItem.getInventoryID()) {
				i.setItemID(invItem.getInventoryID());
				i.setStatus(invItem.getStatus());
				i.setStatusDate(invItem.getStatusDate());
				result = true;
			}
		}
		
		return result;
	}
	
	//************************************************************
	//					INTERNAL METHODS
	//************************************************************

	private void initDb(){
//		inventoryList = new ArrayList<InventoryItem>();
		
		dbArrayList = new ArrayList<DBObj>();
		ArrayList<Item> items = initItems();
		for(Item item: items){
			dbArrayList.add(new DBObj(getIdCount(), item));
		}
		
		dbInventoryArrayList = new ArrayList<DBObj>();
		initInventoryItems();
	}
	
	private void initInventoryItems(){
		int id = getIdCount();
		
		dbInventoryArrayList.add(new DBObj(id, new Inventory(6, Status.SoldOnEtsy, id, "2014-07-10", 10.00)));
		
		id = getIdCount();
		dbInventoryArrayList.add(new DBObj(id, new Inventory(6, Status.Available, id, "2014-07-10", 15.00)));
		
		id = getIdCount();
		dbInventoryArrayList.add(new DBObj(id, new Inventory(6, Status.Lost, id, "2014-07-10", 20.00)));
		
		id = getIdCount();
		dbInventoryArrayList.add(new DBObj(id, new Inventory(6, Status.SoldOnEtsy, id, "2014-07-10", 25.00)));
		
		id = getIdCount();
		dbInventoryArrayList.add(new DBObj(id, new Inventory(6, Status.SoldOnEtsy, id, "2014-07-10", 30.00)));
		
		id = getIdCount();
		dbInventoryArrayList.add(new DBObj(id, new Inventory(5, Status.Available, id, "2014-07-10", 35.00)));
		
		id = getIdCount();
		dbInventoryArrayList.add(new DBObj(id, new Inventory(4, Status.Lost, id, "2014-07-10", 40.00)));
	}
	
	private ArrayList<Item> initItems(){
		ArrayList<Item> result = new ArrayList<Item>();
		ArrayList<Material> materials = new ArrayList<Material>();
		
		materials.add(new Material("Leather", 25.00));
		materials.add(new Material("SketchPaper", 10.00));
		result.add(new Item("Journal", "Journal", materials, 123.12));
		
		materials = new ArrayList<Material>();
		materials.add(new Material("Wood", 10.00));
		result.add(new Item("Diary", "Floppy Disk Notebook", materials, 0.5));
		
		materials = new ArrayList<Material>();
		materials.add(new Material("Duct Tape", 0.50));
		result.add(new Item("Card Sleeve", "RFID Card Sleeve", materials, 12.5));
		
		materials = new ArrayList<Material>();
		materials.add(new Material("Wood", 10.00));
		materials.add(new Material("Buckle", 2.00));
		result.add(new Item("Binder", "Wood Book", materials, 52.5));
		
		materials = new ArrayList<Material>();
		materials.add(new Material("Thread", 3.00));
		materials.add(new Material("Leather", 25.00));
		result.add(new Item("Book Cover", "Sketchbook", materials, 2.46));
		
		materials = new ArrayList<Material>();
		materials.add(new Material("Thread", 3.00));
		result.add(new Item("Book Mark", "Journal", materials, 7.54));
		
		
		materials = new ArrayList<Material>();
		materials.add(new Material("Leather", 25.00));
		materials.add(new Material("SketchPaper", 10.00));
		materials.add(new Material("floppy Disk", 0.50));
		materials.add(new Material("Duct Tape", 0.50));
		result.add(new Item("Agenda", "Keychain Book", materials, 10.57));
		
		return result;
	}
		
	//gets the next id available
	private static int getIdCount(){
		return idCount++;
	}
	
	private DBObj getItemById(int id){
		DBObj result = null;
		
		for(int i = 0; i < dbArrayList.size() && result == null; i++){
			if(dbArrayList.get(i).getId() == id){
				result = dbArrayList.get(i);
			}
		}
		
		return result;
	}
	
	private DBObj getItemByData(Item data){
		DBObj result = null;
		
		for(int i = 0; i < dbArrayList.size() && result == null; i++){
			if( ((DBObj)dbArrayList.get(i)).equals(data) ){
				result = dbArrayList.get(i);
			}
		}
		
		return result;
	}

	public DBObj getInventoryItemById(int id){
		DBObj result = null;
		
		for(int i = 0; i < dbInventoryArrayList.size() && result == null; i++){
			if(dbInventoryArrayList.get(i).getId() == id){
				result = dbInventoryArrayList.get(i);
			}
		}
		
		return result;
	}
	
	private DBObj getItemByData(Inventory data){
		DBObj result = null;
		
		for(int i = 0; i < dbArrayList.size() && result == null; i++){
			if( ((DBObj)dbArrayList.get(i)).equals(data) ){
				result = dbArrayList.get(i);
			}
		}
		
		return result;
	}
	
	/************************************
	 * 	Methods used in Unit tests
	 ************************************/
	
	public boolean emptyInventory() {
		dbInventoryArrayList = new ArrayList<DBObj>();
		return true;
	}
	
	public boolean emptyItems() {
		dbArrayList = new ArrayList<DBObj>();
		return true;
	}

	@Override
	public boolean changeStatus(int id, String status) {
		boolean result = false;
		
		((Inventory)getInventoryItemById(id).getData()).setStatus(status);
		
		return result;
	}
	
//	public ArrayList<InventoryItem> getInventoryItems(){
//		return inventoryList;
//	}
//	
//	public void addInventoryItem(InventoryItem item) {
//		inventoryList.add(item);
//	}
}
