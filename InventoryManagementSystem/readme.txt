After unzipping the file, there are 3 directories:

	lib - contains the required libraries such as junit and swt
	bin - contains the .class files
	src - contains the .java source files

The structure of  \bin and \lib are identical:

	Application\ - Contains the main executable
	BusinessLogic\ - Contains the Business Logic classes
	Database\    - Contains the DB Stub && Real Database
	DomainObj\    - Contains the domain objects
	InventoryListPage\ - contains the GUI components of our system
	Tests\ - Unit Tests

The architecture diagram can be found in the file Architecture Diagram.png

Our source control can be found at: https://bitbucket.org/ddao11/3350-project

IMPORTANT NOTE ABOUT ACCEPTANCE TESTS:
---------------------------------------
When we went to implement automatic acceptance tests using ATR, we came across a startling discovery.
ATR can integrate with a few SWT widgets, however, there were three design implementations we have
which did not work with ATR at all:

	1) We used internal Table checkboxes, which ATR could not recognize.
	2) We used an abstract "Master" page to be a template for other pages. ATR couldn't communicate
		from the child class to the parent class' widgets
	3) We used DatePicker widgets, which are not recognized by ATR at all.
	
Due to these issues, much of our program is untestable by ATR, and we were faced with a choice:
implement only those test which were possible, or re-design all our GUIs. We opted for the former due
to time restraints, and realize this is far from optimal.

In the future, we feel it would be helpful to let people know which SWT widgets are useable by ATR,
so the design of GUIs can comply early on in development. This is a good example of a "Waterfall" 
design weakness - it is hard to change things after they are already in use. Had we had "Feedback" 
after the first iteration about the GUI design we had, we could have made the change early on and 
then used ATR for our Acceptance tests.

It was not for lack of effort on our part that ATR scripts are scarce in our project, as can be witnessed
by the imports and the registers in all the GUI constructors. We simply did not have the time to 
completely redesign multiple classes in the short amount of time allotted to this iteration, especially since
we focussed on finishing the development before the Acceptance tests.

We hope that this will be taken into consideration when the marks are calculated.

Thank-you,

Group 7 
